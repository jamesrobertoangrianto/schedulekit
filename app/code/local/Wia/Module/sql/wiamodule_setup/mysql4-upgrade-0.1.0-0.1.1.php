<?php
echo 'Testing our upgrade script (0.1.1) and NOT halting execution <br />';

$installer = new Mage_Sales_Model_Mysql4_Setup('core_setup');
    
$installer->startSetup();
    
$assign_member_id  = array(
    'type'          => 'text',
    'backend_type'  => 'text',
    'frontend_input' => 'text',
    'is_user_defined' => true,
    'label'         => 'assign_member_id',
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,   
    'searchable'    => false,
    'filterable'    => false,
    'comparable'    => false,
    'default'       => ''
);
$installer->addAttribute('order', 'assign_member_id', $assign_member_id);
$installer->addAttribute('quote', 'assign_member_id', $assign_member_id);



$to_date  = array(
    'type'          => 'text',
    'backend_type'  => 'text',
    'frontend_input' => 'text',
    'is_user_defined' => true,
    'label'         => 'to_date',
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,   
    'searchable'    => false,
    'filterable'    => false,
    'comparable'    => false,
    'default'       => ''
);
$installer->addAttribute('order', 'to_date', $to_date);
$installer->addAttribute('quote', 'to_date', $to_date);





$from_date  = array(
    'type'          => 'text',
    'backend_type'  => 'text',
    'frontend_input' => 'text',
    'is_user_defined' => true,
    'label'         => 'from_date',
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,   
    'searchable'    => false,
    'filterable'    => false,
    'comparable'    => false,
    'default'       => ''
);
$installer->addAttribute('order', 'from_date', $from_date);
$installer->addAttribute('quote', 'from_date', $from_date);





$booking_channel  = array(
    'type'          => 'text',
    'backend_type'  => 'text',
    'frontend_input' => 'text',
    'is_user_defined' => true,
    'label'         => 'booking_channel',
    'visible'       => true,
    'required'      => false,
    'user_defined'  => false,   
    'searchable'    => false,
    'filterable'    => false,
    'comparable'    => false,
    'default'       => ''
);
$installer->addAttribute('order', 'booking_channel', $from_date);
$installer->addAttribute('quote', 'booking_channel', $from_date);

    
    $installer->endSetup();

?>