<?php
echo 'Testing our upgrade script (0.1.3) and NOT halting execution <br />';


$installer = $this;
$installer->startSetup();
$table = $installer->getConnection()->newTable($installer->getTable('Module/business'))
    ->addColumn('business_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'identity' => true,
        ), 'business ID')

    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        ), 'business name')

        ->addColumn('business_owner_id', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
            ), 'business_owner_id')

    ->addColumn('category', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        ), 'business category')
    ->addColumn('type', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        ), 'business type')
    ->addColumn('phone', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        ), 'business phone')

        ->addColumn('website', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
            ), 'business website')

   

    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_LONGVARCHAR, null, array(
        'nullable' => false,
        ), 'business description')

        ->addColumn('short_description', Varien_Db_Ddl_Table::TYPE_LONGVARCHAR, null, array(
            'nullable' => false,
            ), 'business short_description')      
    

    ->addColumn('date', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        ), 'business Date')
    ->addColumn('timestamp', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Timestamp')

    ->setComment('Magentotutorial weblog/blogpost entity table');
$installer->getConnection()->createTable($table);

$installer->endSetup();


?>
