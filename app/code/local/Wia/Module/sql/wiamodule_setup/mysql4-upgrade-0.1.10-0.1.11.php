<?php
echo 'Testing our upgrade script (0.1.11) and NOT halting execution <br />';


$installer = $this;
$installer->startSetup();
$table = $installer->getConnection()->newTable($installer->getTable('Module/businesshours'))
    ->addColumn('business_hours_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'identity' => true,
        ), 'business_hours_id')

        ->addColumn('business_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            ), 'customer business_id')

        ->addColumn('start_time', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        ), 'start_time')

        ->addColumn('end_time', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
        ), 'end_time')

        ->addColumn('day', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
                'nullable' => false,
                ), 'day')

        ->addColumn('timestamp', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Timestamp')

    ->setComment('Magentotutorial weblog/blogpost entity table');
$installer->getConnection()->createTable($table);

$installer->endSetup();


?>
