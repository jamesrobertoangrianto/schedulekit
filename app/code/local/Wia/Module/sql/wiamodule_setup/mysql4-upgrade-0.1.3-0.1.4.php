<?php
echo 'Testing our upgrade script (0.1.4) and NOT halting execution <br />';


$installer = $this;
$installer->startSetup();
$table = $installer->getConnection()->newTable($installer->getTable('Module/address'))
    ->addColumn('address_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'identity' => true,
        ), 'Address ID')

        ->addColumn('business_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            ), 'address business_id')

        ->addColumn('city', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        ), 'address city')

        ->addColumn('telephone', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
            ), 'address telephone')

            ->addColumn('email', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
                'nullable' => false,
                ), 'address email')

        ->addColumn('street', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        ), 'business street')

        ->addColumn('region', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        ), 'business region')

        ->addColumn('coordinates', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        ), 'business coordinates')

        ->addColumn('timestamp', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Timestamp')

    ->setComment('Magentotutorial weblog/blogpost entity table');
$installer->getConnection()->createTable($table);

$installer->endSetup();


?>
