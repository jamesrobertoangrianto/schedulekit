<?php
echo 'Testing our upgrade script (0.1.8) and NOT halting execution <br />';


$installer = $this;
$installer->startSetup();
$table = $installer->getConnection()->newTable($installer->getTable('Module/review'))
    ->addColumn('review_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'identity' => true,
        ), 'review_id')

        ->addColumn('business_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            ), 'gallery business_id')

        ->addColumn('rating', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
        ), 'rating')

        ->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
                'nullable' => false,
                ), 'title')

        ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
            ), 'description')

        ->addColumn('response', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
                'nullable' => false,
                ), 'response')
        
        ->addColumn('verified', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
                        'nullable' => false,
                        ), 'response')
        
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
                'nullable' => false,
                ), 'status')

        ->addColumn('timestamp', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Timestamp')

    ->setComment('Magentotutorial weblog/blogpost entity table');
$installer->getConnection()->createTable($table);

$installer->endSetup();


?>
