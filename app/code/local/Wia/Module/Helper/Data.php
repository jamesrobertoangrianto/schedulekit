<?php
class Wia_Module_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function resizeImage($fullPath, $fileName, $width, $height, $quality){
        // $folderURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $imageURL = $fullPath;

        $category = 0;

        $ProductPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'product' . $fileName;
        $CategoryPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'category' . DS . $fileName;
        $ProfilePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS  . 'host' . $fileName;

        $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'cache' . DS . $width . 'x' . $height . $fileName;
        $newCategoryPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'cache' . DS . $width . 'x' . $height . DS . $fileName;
        $newProfilePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'host' . DS . 'cache' . DS . $width . 'x' . $height . DS . $fileName;
        //if width empty then return original size image's URL

            if ($width != '') {
                //if image has already resized then just return URL
                if (file_exists($CategoryPath) && is_file($CategoryPath) && !file_exists($newCategoryPath)) { 
                    $imageObj = new Varien_Image($CategoryPath);
                    $imageObj->constrainOnly(TRUE);
                    $imageObj->keepAspectRatio(FALSE);
                    $imageObj->keepFrame(FALSE);
                    $imageObj->quality ( $quality );
                    $imageObj->resize($width, $height);
                    $imageObj->save($newCategoryPath);
                    // imagewebp(imagecreatefrompng($read_from_path), $save_to_path + ".webp");

                    $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'cache' . DS . $width . 'x' . $height . DS . $fileName;
                }
                else if(file_exists($ProductPath) && is_file($ProductPath) && !file_exists($newPath)){
                    $imageObj = new Varien_Image($ProductPath);
                    $imageObj->constrainOnly(TRUE);
                    $imageObj->keepAspectRatio(FALSE);
                    $imageObj->keepFrame(FALSE);
                    $imageObj->quality ( $quality );
                    $imageObj->resize($width, $height);
                    $imageObj->save($newPath);

                    $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'cache' . DS . $width . 'x' . $height . $fileName;
                }
                else if(file_exists($ProfilePath) && is_file($ProfilePath) && !file_exists($newProfilePath)){
                    $imageObj = new Varien_Image($ProfilePath);
                    $imageObj->constrainOnly(TRUE);
                    $imageObj->keepAspectRatio(FALSE);
                    $imageObj->keepFrame(FALSE);
                    $imageObj->quality ( $quality );
                    $imageObj->resize($width, $height);
                    $imageObj->save($newProfilePath);

                    $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'host' . DS . 'cache' . DS . $width . 'x' . $height . DS . $fileName;
                }
                else if(file_exists($newCategoryPath)){
                    $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'cache' . DS . $width . 'x' . $height . DS . $fileName;
                }
                else if(file_exists($newProfilePath)){
                    $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'host' . DS . 'cache' . DS . $width . 'x' . $height . DS . $fileName;
                }
                else {
                    $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'cache' . DS . $width . 'x' . $height . $fileName;
                }
            } 
            else {
                $resizedURL = $imageURL;
            }
        // return imagewebp($resizedURL, 'php.webp');
        return $resizedURL;
    }

    public function CatalogImage($fullPath, $fileName, $width, $height, $quality){
        // $folderURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $imageURL = $fullPath;
        // $basePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . '/catalog/product' . $fileName;
        $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . "/catalog/cache" . DS . $fileName;
        //if width empty then return original size image's URL

            if ($width != '') {
                //if image has already resized then just return URL
                if (file_exists($fullPath) && is_file($fullPath) && !file_exists($newPath)) {
                    $imageObj = new Varien_Image($fullPath);
                    $imageObj->constrainOnly(TRUE);
                    $imageObj->keepAspectRatio(FALSE);
                    $imageObj->keepFrame(FALSE);
                    $imageObj->quality ( $quality );
                    $imageObj->resize($width, $height);
                    $imageObj->save($newPath);
                }
                $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "/catalog/cache" . DS . $fileName;
            } 
            else {
                $resizedURL = $imageURL;
            }
        return $resizedURL;
    }

    public function CategoryImage($fileName, $width, $height, $quality){
        $folderURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $imageURL = $folderURL . $fileName;
        $basePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . '/catalog/category' . $fileName;
        $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . "/catalog/cache" . DS . $fileName;
        //if width empty then return original size image's URL

            if ($width != '') {
                //if image has already resized then just return URL
                if (file_exists($basePath) && is_file($basePath) && !file_exists($newPath)) {
                    $imageObj = new Varien_Image($basePath);
                    $imageObj->constrainOnly(TRUE);
                    $imageObj->keepAspectRatio(FALSE);
                    $imageObj->keepFrame(FALSE);
                    $imageObj->quality ( $quality );
                    $imageObj->resize($width, $height);
                    $imageObj->save($newPath);
                }
                $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "resized" . DS . $fileName;
            } 
            else {
                $resizedURL = $imageURL;
            }
        return $resizedURL;
    }

    public function getResizedImage($image, $width, $height, $quality) {
        // if (!$this->getImage ())
        //     return false;
    
        $imageUrl = Mage::getBaseDir ( 'media' ) . DS . "catalog" . DS . "category" . DS . $image;
        if (! is_file ( $imageUrl ))
            return false;
    
        $imageResized = Mage::getBaseDir ( 'media' ) . DS . "catalog" . DS . "product" . DS . "cache" . DS . "cat_resize" . DS . $image;// Because clean Image cache function works in this folder only
        if (! file_exists ( $imageResized ) && file_exists ( $imageUrl ) || file_exists($imageUrl) && filemtime($imageUrl) > filemtime($imageResized)) :
            $imageObj = new Varien_Image ( $imageUrl );
            $imageObj->constrainOnly ( true );
            $imageObj->keepAspectRatio ( true );
            $imageObj->keepFrame ( true );
            $imageObj->quality ( $quality );
            $imageObj->resize ( $width, $height );
            $imageObj->save ( $imageResized );
        endif;
    
        if(file_exists($imageResized)){
            return Mage::getBaseUrl ( 'media' ) ."catalog/product/cache/cat_resize/" . $image;
        }else{
            return $this->getImageUrl();
        }
    
    }

    public function toWebp($image, $width, $height, $quality) {
      // if (!$this->getImage ())
      //     return false;
  
      $imageUrl = Mage::getBaseDir ( 'media' ) . DS . "catalog" . DS . "category" . DS . $image;
      if (! is_file ( $imageUrl ))
          return false;
  
      $imageResized = Mage::getBaseDir ( 'media' ) . DS . "catalog" . DS . "product" . DS . "cache" . DS . "cat_resize" . DS . $image;// Because clean Image cache function works in this folder only
      if (! file_exists ( $imageResized ) && file_exists ( $imageUrl ) || file_exists($imageUrl) && filemtime($imageUrl) > filemtime($imageResized)) :
          $imageObj = new Varien_Image ( $imageUrl );
          $imageObj->constrainOnly ( true );
          $imageObj->keepAspectRatio ( true );
          $imageObj->keepFrame ( true );
          $imageObj->quality ( $quality );
          $imageObj->resize ( $width, $height );
          $imageObj->save ( $imageResized );
      endif;
  
      if(file_exists($imageResized)){
          return Mage::getBaseUrl ( 'media' ) ."catalog/product/cache/cat_resize/" . $image;
      }else{
          return $this->getImageUrl();
      }
  
  }

    public function Payment($data) {
        // $params = $this->getRequest()->getParams();
        $orderIncrementId = $data;
        
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        $sessionId = Mage::getSingleton('core/session');

        $shipping_amount     = $order->getShippingAmount();
        $shipping_tax_amount = $order->getShippingTaxAmount();
        $discount_amount = $order->getDiscountAmount();

        foreach ($order->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
          $item_array[] = array(
            // "id" => $item->getProductId(),
            // "price" => $item->getPrice() * 1,
            // "quantity" => $order->getTotalQtyOrdered() * 1,
            // "name" => $item->getName()


            'id'       => $item->getProductId(),
            'price'    => number_format($item->getPrice(), 0, '.', ''),
            // $item->getPrice(),
            'quantity' => $item->getQtyToInvoice(),
            'name'     => substr($item->getName(),0,30),
            
          );
        }

        if ($order->getDiscountAmount() != 0) {
            $couponItem = array(
                'id' => 'DISCOUNT',
                'price' => number_format($order->getDiscountAmount(), 0, '.', ''),
                // $order->getDiscountAmount(),
                'quantity' => 1,
                'name' => 'DISCOUNT'
              );
              $item_array[] = $couponItem;
          }

        if ($shipping_amount > 0) {
          $shipping_item = array(
              'id' => 'SHIPPING',
              'price' => number_format($shipping_amount, 0, '.', ''),
            //   $shipping_amount,
              'quantity' => 1,
              'name' => 'Shipping Cost'
            );
          $item_array[] = $shipping_item;
        }
        
        if ($shipping_tax_amount > 0) {
          $shipping_tax_item = array(
              'id' => 'SHIPPING_TAX',
              'price' => number_format($shipping_tax_amount, 0, '.', ''),
            //   $shipping_tax_amount,
              'quantity' => 1,
              'name' => 'Shipping Tax'
            );
          $item_array[] = $shipping_tax_item;
        }
        
        if ($discount_amount > 0) {
            $discount = array(
                'id' => 'DISCOUNT',
                'price' => number_format($discount_amount, 0, '.', ''),
                // $discount_amount,
                'quantity' => 1,
                'name' => 'Discount'
              );
            $item_array[] = $discount;
          }

        // // convert to IDR
        // $current_currency = Mage::app()->getStore()->getCurrentCurrencyCode();
        // if ($current_currency != 'IDR') {
        // $conversion_func = function ($non_idr_price) {
        //         return $non_idr_price *
        //             Mage::getStoreConfig('payment/snapio/conversion_rate');
        //     };
        //     foreach ($item_array as $item) {
        //         $item['price'] = intval(round(call_user_func($conversion_func, $item['price'])));
        //     }
        //     unset($item);
        // }
        // else {
        //     foreach ($item_array as $each) {
        //         $each['price'] = (int) $each['price'];
        //     }
        //     unset($each);
        // }

        $order_shipping_address = $order->getShippingAddress();
        $order_billing_address = $order->getBillingAddress();
    
        $config = json_encode(array(
        // $config = array(
          // "payment_type" => "bank_transfer",
          "transaction_details" => [
              "order_id" => $orderIncrementId,
              "gross_amount" => number_format($order->getGrandTotal(), 0, '.', '')
            //   $order->getGrandTotal() 
          ],
         
          "item_details" => $item_array,
          "credit_card" => [
            "secure" => true,
            // "channel" => "migs",
            // "bank" => "bca",
            // "installment"=> [
            //   "required"=> false,
            //   "terms"=> [
            //     "bni"=> [3, 6,12],
            //     "mandiri"=> [3, 6,12],
            //     "cimb"=> [3,6,12],
            //     "bca" => [3, 6,12],
            //     "offline" => [3,6,12]
            //   ]
            // ]
           
              ],
          "customer_details" => [
            "first_name" => $order_shipping_address->getFirstname(),
            "last_name" => $order_shipping_address->getLastname(),
            "email" => $order_shipping_address->getEmail(),
            "phone" => $order_shipping_address->getTelephone(),
            "billing_address" => [
              "first_name" => $order_billing_address->getFirstname(),
              "last_name" => $order_billing_address->getLastname(),
              "email" => $order_billing_address->getEmail(),
              "phone" => $order_billing_address->getTelephone(),
              "address" => $order_billing_address->getStreet(1),
              "city" => $order_billing_address->getCity(),
              "postal_code" => $order_billing_address->getPostcode(),
              "country_code" => $this->convert_country_code($order_billing_address->getCountry())
            ],
            "shipping_address" => [
              "first_name" => $order_shipping_address->getFirstname(),
              "last_name" => $order_shipping_address->getLastname(),
              "email" => $order_shipping_address->getEmail(),
              "phone" => $order_shipping_address->getTelephone(),
              "address" => $order_shipping_address->getStreet(1),
              "city" => $order_shipping_address->getCity(),
              "postal_code" => $order_shipping_address->getPostcode(),
              "country_code" => $this->convert_country_code($order_shipping_address->getCountry())
            ]
          ]
        )
        );
        
        try {
    
            $url    = 'https://app.midtrans.com/snap/v1/transactions';
            // $url    = 'https://app.sandbox.midtrans.com/snap/v1/transactions';
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            //WIA SANDBOX SERVER KEY
            // curl_setopt($curl, CURLOPT_HTTPHEADER,array(
            //     "Accept: application/json",
            //     "Content-type: application/json",
            //     "Authorization: Basic ".base64_encode("SB-Mid-server-s9yxKYsHYJkETlq9iAlMin4z:")));
      
            //LOCAL SANDBOX SERVER KEY
            // curl_setopt($curl, CURLOPT_HTTPHEADER,array(
            //     "Accept: application/json",
            //     "Content-type: application/json",
            //     "Authorization: Basic U0ItTWlkLXNlcnZlci1SNWZqSXhVWHZBeU5fcktSYW9NN3VqNHY6"));
            // VT-server-eXDdHdxyRhlGNm1m6r1h_xEt
            //WIA PRODUCTION SERVER KEY
            curl_setopt($curl, CURLOPT_HTTPHEADER,array(
                "Accept: application/json",
                "Content-type: application/json",
                "Authorization: Basic ".base64_encode("Mid-server-v1FHFarD32nIbLMzl30xpHDy:")));
    
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
            
                $result     = curl_exec($curl);
                $array = Mage::helper('core')->jsondecode($result);
                curl_close($curl);
    
        //   $this->_sendAPI($config);
        return $array;
    
          //remove item
          // foreach( Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection() as $item ){
          //       Mage::getSingleton('checkout/cart')->removeItem( $item->getId() )->save();
          // }
          // Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl().'snapio/payment/opensnap');
    
        }
        catch (Exception $e) {
          // error_log($e->getMessage());
          // Mage::log('error:'.print_r($e->getMessage(),true),null,'snap.log',true);
        }
    }

    public function Payments($data) {
      error_log('masuk redirect action');
      $orderIncrementId = $data;
        
    //  $orderIncrementId = $this->_getCheckout()->getLastRealOrderId();
      $order = Mage::getModel('sales/order')
          ->loadByIncrementId($orderIncrementId);
     
      $transaction_details = array();
      $transaction_details['order_id'] = $orderIncrementId;
      $transaction_details['gross_amount'] = $order->getGrandTotal();
      
      $order_billing_address = $order->getBillingAddress();
      $billing_address = array();
      $billing_address['first_name']   = $order_billing_address->getFirstname();
      $billing_address['last_name']    = $order_billing_address->getLastname();
      $billing_address['address']      = $order_billing_address->getStreet(1);
      $billing_address['city']         = $order_billing_address->getCity();
      $billing_address['postal_code']  = $order_billing_address->getPostcode();
      $billing_address['country_code'] = $this->convert_country_code($order_billing_address->getCountry());
      $billing_address['phone']        = $order_billing_address->getTelephone();
  
      $order_shipping_address = $order->getShippingAddress();
      $shipping_address = array();
      $shipping_address['first_name']   = $order_shipping_address->getFirstname();
      $shipping_address['last_name']    = $order_shipping_address->getLastname();
      $shipping_address['address']      = $order_shipping_address->getStreet(1);
      $shipping_address['city']         = $order_shipping_address->getCity();
      $shipping_address['postal_code']  = $order_shipping_address->getPostcode();
      $shipping_address['phone']        = $order_shipping_address->getTelephone();
      $shipping_address['country_code'] =
          $this->convert_country_code($order_shipping_address->getCountry());
  
      $customer_details = array();
      $customer_details['billing_address']  = $billing_address;
      $customer_details['shipping_address'] = $shipping_address;
      $customer_details['first_name']       = $order_billing_address
          ->getFirstname();
      $customer_details['last_name']        = $order_billing_address
          ->getLastname();
      $customer_details['email']            = $order_billing_address->getEmail();
      $customer_details['phone']            = $order_billing_address
          ->getTelephone();
  
      $items               = $order->getAllItems();
      $shipping_amount     = $order->getShippingAmount();
      $shipping_tax_amount = $order->getShippingTaxAmount();
      $tax_amount = $order->getTaxAmount();
  
      $item_details = array();
  
  
      foreach ($items as $each) {
        $item = array(
            'id'       => $each->getProductId(),
            'price'    => $each->getPrice(),
            'quantity' => $each->getQtyToInvoice(),
            'name'     => $each->getName()
          );
        
        if ($item['quantity'] == 0) continue;
        // error_log(print_r($each->getProductOptions(), true));
        $item_details[] = $item;
      }
      
      $num_products = count($item_details);
  
      unset($each);
  
      if ($order->getDiscountAmount() != 0) {
        $couponItem = array(
            'id' => 'DISCOUNT',
            'price' => $order->getDiscountAmount(),
            'quantity' => 1,
            'name' => 'DISCOUNT'
          );
        $item_details[] = $couponItem;
      }
  
      if ($shipping_amount > 0) {
        $shipping_item = array(
            'id' => 'SHIPPING',
            'price' => $shipping_amount,
            'quantity' => 1,
            'name' => 'Shipping Cost'
          );
        $item_details[] =$shipping_item;
      }
      
      if ($shipping_tax_amount > 0) {
        $shipping_tax_item = array(
            'id' => 'SHIPPING_TAX',
            'price' => $shipping_tax_amount,
            'quantity' => 1,
            'name' => 'Shipping Tax'
          );
        $item_details[] = $shipping_tax_item;
      }
  
      if ($tax_amount > 0) {
        $tax_item = array(
            'id' => 'TAX',
            'price' => $tax_amount,
            'quantity' => 1,
            'name' => 'Tax'
          );
        $item_details[] = $tax_item;
      }
  
     
      //$bin = explode(',', Mage::getStoreConfig('payment/snapio/bin'));
     // $credit_card['save_card'] = true;
      $credit_card['secure'] = true;
      //$credit_card['whitelist_bins'] = $bin;
      
      // $installment = array();
      // $installment_term = array();  
      // $installment_term['bni'] = array(3,6,12);
      // $installment_term['mandiri'] = array(3,6,12);
      // $installment_term['cimb'] = array(3,6,12);
      // $installment_term['bca'] = array(3,6,12);
      // $installment_term['offline'] = array(3,6,12);
  
  
      // $installment['required'] = FALSE;
      // $installment['terms'] = $installment_term;    
  
      // $credit_card['installment'] = $installment;
  
  
      $payloads = array();
      $payloads['transaction_details'] = $transaction_details;
      //$payloads['enabled_payments']    = array('credit_card');
      $payloads['item_details']        = $item_details;
      $payloads['customer_details']    = $customer_details;    
  
    
  
      Mage::log(json_encode($payloads),null,'snap.log',true);
      $config = json_encode($payloads);
      try {
    
        $url    = 'https://app.midtrans.com/snap/v1/transactions';
        //$url    = 'https://app.sandbox.midtrans.com/snap/v1/transactions';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($curl, CURLOPT_HTTPHEADER,array(
            "Accept: application/json",
            "Content-type: application/json",
            "Authorization: Basic ".base64_encode("Mid-server-v1FHFarD32nIbLMzl30xpHDy:")));

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
            $result     = curl_exec($curl);
            $array = Mage::helper('core')->jsondecode($result);
            curl_close($curl);

    //   $this->_sendAPI($config);
          return $array;

            //remove item
            // foreach( Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection() as $item ){
            //       Mage::getSingleton('checkout/cart')->removeItem( $item->getId() )->save();
            // }
            // Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl().'snapio/payment/opensnap');

          }
          catch (Exception $e) {
            // error_log($e->getMessage());
            // Mage::log('error:'.print_r($e->getMessage(),true),null,'snap.log',true);
          }
    }
    
      public function opensnapAction(){
          
          $template = 'snapio/open.phtml';
    
          //Get current layout state
          $this->loadLayout();          
          
          $block = $this->getLayout()->createBlock(
              'Mage_Core_Block_Template',
              'snap',
              array('template' => $template)
          );
         
          $this->getLayout()->getBlock('root')->setTemplate('page/empty.phtml');
          
          
          $this->getLayout()->getBlock('content')->append($block);
          
          $this->_initLayoutMessages('core/session'); 
          $this->renderLayout();
      }
    
      /**
       * Convert 2 digits coundry code to 3 digit country code
       *
       * @param String $country_code Country code which will be converted
       */
      public function convert_country_code( $country_code ) {
    
        // 3 digits country codes
        $cc_three = array(
          'AF' => 'AFG',
          'AX' => 'ALA',
          'AL' => 'ALB',
          'DZ' => 'DZA',
          'AD' => 'AND',
          'AO' => 'AGO',
          'AI' => 'AIA',
          'AQ' => 'ATA',
          'AG' => 'ATG',
          'AR' => 'ARG',
          'AM' => 'ARM',
          'AW' => 'ABW',
          'AU' => 'AUS',
          'AT' => 'AUT',
          'AZ' => 'AZE',
          'BS' => 'BHS',
          'BH' => 'BHR',
          'BD' => 'BGD',
          'BB' => 'BRB',
          'BY' => 'BLR',
          'BE' => 'BEL',
          'PW' => 'PLW',
          'BZ' => 'BLZ',
          'BJ' => 'BEN',
          'BM' => 'BMU',
          'BT' => 'BTN',
          'BO' => 'BOL',
          'BQ' => 'BES',
          'BA' => 'BIH',
          'BW' => 'BWA',
          'BV' => 'BVT',
          'BR' => 'BRA',
          'IO' => 'IOT',
          'VG' => 'VGB',
          'BN' => 'BRN',
          'BG' => 'BGR',
          'BF' => 'BFA',
          'BI' => 'BDI',
          'KH' => 'KHM',
          'CM' => 'CMR',
          'CA' => 'CAN',
          'CV' => 'CPV',
          'KY' => 'CYM',
          'CF' => 'CAF',
          'TD' => 'TCD',
          'CL' => 'CHL',
          'CN' => 'CHN',
          'CX' => 'CXR',
          'CC' => 'CCK',
          'CO' => 'COL',
          'KM' => 'COM',
          'CG' => 'COG',
          'CD' => 'COD',
          'CK' => 'COK',
          'CR' => 'CRI',
          'HR' => 'HRV',
          'CU' => 'CUB',
          'CW' => 'CUW',
          'CY' => 'CYP',
          'CZ' => 'CZE',
          'DK' => 'DNK',
          'DJ' => 'DJI',
          'DM' => 'DMA',
          'DO' => 'DOM',
          'EC' => 'ECU',
          'EG' => 'EGY',
          'SV' => 'SLV',
          'GQ' => 'GNQ',
          'ER' => 'ERI',
          'EE' => 'EST',
          'ET' => 'ETH',
          'FK' => 'FLK',
          'FO' => 'FRO',
          'FJ' => 'FJI',
          'FI' => 'FIN',
          'FR' => 'FRA',
          'GF' => 'GUF',
          'PF' => 'PYF',
          'TF' => 'ATF',
          'GA' => 'GAB',
          'GM' => 'GMB',
          'GE' => 'GEO',
          'DE' => 'DEU',
          'GH' => 'GHA',
          'GI' => 'GIB',
          'GR' => 'GRC',
          'GL' => 'GRL',
          'GD' => 'GRD',
          'GP' => 'GLP',
          'GT' => 'GTM',
          'GG' => 'GGY',
          'GN' => 'GIN',
          'GW' => 'GNB',
          'GY' => 'GUY',
          'HT' => 'HTI',
          'HM' => 'HMD',
          'HN' => 'HND',
          'HK' => 'HKG',
          'HU' => 'HUN',
          'IS' => 'ISL',
          'IN' => 'IND',
          'ID' => 'IDN',
          'IR' => 'RIN',
          'IQ' => 'IRQ',
          'IE' => 'IRL',
          'IM' => 'IMN',
          'IL' => 'ISR',
          'IT' => 'ITA',
          'CI' => 'CIV',
          'JM' => 'JAM',
          'JP' => 'JPN',
          'JE' => 'JEY',
          'JO' => 'JOR',
          'KZ' => 'KAZ',
          'KE' => 'KEN',
          'KI' => 'KIR',
          'KW' => 'KWT',
          'KG' => 'KGZ',
          'LA' => 'LAO',
          'LV' => 'LVA',
          'LB' => 'LBN',
          'LS' => 'LSO',
          'LR' => 'LBR',
          'LY' => 'LBY',
          'LI' => 'LIE',
          'LT' => 'LTU',
          'LU' => 'LUX',
          'MO' => 'MAC',
          'MK' => 'MKD',
          'MG' => 'MDG',
          'MW' => 'MWI',
          'MY' => 'MYS',
          'MV' => 'MDV',
          'ML' => 'MLI',
          'MT' => 'MLT',
          'MH' => 'MHL',
          'MQ' => 'MTQ',
          'MR' => 'MRT',
          'MU' => 'MUS',
          'YT' => 'MYT',
          'MX' => 'MEX',
          'FM' => 'FSM',
          'MD' => 'MDA',
          'MC' => 'MCO',
          'MN' => 'MNG',
          'ME' => 'MNE',
          'MS' => 'MSR',
          'MA' => 'MAR',
          'MZ' => 'MOZ',
          'MM' => 'MMR',
          'NA' => 'NAM',
          'NR' => 'NRU',
          'NP' => 'NPL',
          'NL' => 'NLD',
          'AN' => 'ANT',
          'NC' => 'NCL',
          'NZ' => 'NZL',
          'NI' => 'NIC',
          'NE' => 'NER',
          'NG' => 'NGA',
          'NU' => 'NIU',
          'NF' => 'NFK',
          'KP' => 'MNP',
          'NO' => 'NOR',
          'OM' => 'OMN',
          'PK' => 'PAK',
          'PS' => 'PSE',
          'PA' => 'PAN',
          'PG' => 'PNG',
          'PY' => 'PRY',
          'PE' => 'PER',
          'PH' => 'PHL',
          'PN' => 'PCN',
          'PL' => 'POL',
          'PT' => 'PRT',
          'QA' => 'QAT',
          'RE' => 'REU',
          'RO' => 'SHN',
          'RU' => 'RUS',
          'RW' => 'EWA',
          'BL' => 'BLM',
          'SH' => 'SHN',
          'KN' => 'KNA',
          'LC' => 'LCA',
          'MF' => 'MAF',
          'SX' => 'SXM',
          'PM' => 'SPM',
          'VC' => 'VCT',
          'SM' => 'SMR',
          'ST' => 'STP',
          'SA' => 'SAU',
          'SN' => 'SEN',
          'RS' => 'SRB',
          'SC' => 'SYC',
          'SL' => 'SLE',
          'SG' => 'SGP',
          'SK' => 'SVK',
          'SI' => 'SVN',
          'SB' => 'SLB',
          'SO' => 'SOM',
          'ZA' => 'ZAF',
          'GS' => 'SGS',
          'KR' => 'KOR',
          'SS' => 'SSD',
          'ES' => 'ESP',
          'LK' => 'LKA',
          'SD' => 'SDN',
          'SR' => 'SUR',
          'SJ' => 'SJM',
          'SZ' => 'SWZ',
          'SE' => 'SWE',
          'CH' => 'CHE',
          'SY' => 'SYR',
          'TW' => 'TWN',
          'TJ' => 'TJK',
          'TZ' => 'TZA',
          'TH' => 'THA',
          'TL' => 'TLS',
          'TG' => 'TGO',
          'TK' => 'TKL',
          'TO' => 'TON',
          'TT' => 'TTO',
          'TN' => 'TUN',
          'TR' => 'TUR',
          'TM' => 'TKM',
          'TC' => 'TCA',
          'TV' => 'TUV',
          'UG' => 'UGA',
          'UA' => 'UKR',
          'AE' => 'ARE',
          'GB' => 'GBR',
          'US' => 'USA',
          'UY' => 'URY',
          'UZ' => 'UZB',
          'VU' => 'VUT',
          'VA' => 'VAT',
          'VE' => 'VEN',
          'VN' => 'VNM',
          'WF' => 'WLF',
          'EH' => 'ESH',
          'WS' => 'WSM',
          'YE' => 'YEM',
          'ZM' => 'ZMB',
          'ZW' => 'ZWE'
        );
    
        // Check if country code exists
        if( isset( $cc_three[ $country_code ] ) && $cc_three[ $country_code ] != '' ) {
          $country_code = $cc_three[ $country_code ];
        }
    
        return $country_code;
      }

}
?>