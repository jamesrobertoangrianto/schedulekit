<?php
    class Wia_Module_CustomerController extends Mage_Core_Controller_Front_Action
    {

        public function _sendAPI($data){
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
        }

        public function getCutomerSessionAction(){
            
            $session = Mage::getSingleton('customer/session');
                   
            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                // Get the customer object from customer session
                $customer = Mage::getSingleton('customer/session')->getCustomer();
                 
                $custID = $customer->getID();//get customer id
                $custName = $customer->getName();//get customer full name
                $custEmail = $customer->getEmail();//get customer email

                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                    'session_id' => 'session_id',
                    'customer' => array(
                        id=>$custID,
                        name=> $custName,
                        isLogin=>true,

                    )
                );
            }
            else{
                $response = array( 
                    'status_code' => 400,
                    'message_dialog' => 'false',
                    
                    'customer' => array(
                  
                        isLogin=>true,

                    )
                );
            }

           
            
            $this->_sendAPI( $response);
        
        }

        public function LoginAction(){

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);

                $customerEmail = $params['email'];
                $customerPassword = $params['password'];

                if (!empty($customerEmail) && !empty($customerPassword)){

                    $customer = Mage::getModel("customer/customer"); 
                    $customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 
                    $customer->loadByEmail($customerEmail); 

                    //Session
                    $session = Mage::getSingleton('customer/session');
                    //$customerId = $this->_getSession($customer);
                    if($customer->getId()){
                        try{
                        
                            $session->setCustomerAsLoggedIn($customer);
                            $session->login($customerEmail, $customerPassword);
    
                            $encrypted = Mage::helper('core')->encrypt($session->getId());
    
    
                            $apiarray = array(
                                'message_code' => 200, 
                                'message_dialog' => 'Login Success',
                                'customer'=>array(
                                    'customer_id' =>$customer->getId(), 
                                    'business_id' =>$this->_getBusinessProfile($customer->getId()), 
                                    // 'address_id' =>$address_id, 
                                    'token' => $session->getId(),
                                    'isLogin' => true, 
                                    
                                )
                                
                                );
    
                         
                                            
                            $this->_sendAPI($apiarray);
                            
                        }catch (Exception $e){
                            $message_code = array('message_code' => 403, 'message_dialog' => 'Wrong Email or Password');
                            $this->_sendAPI($message_code);
                        } 
                    }
                   
                } 
                else {
                    $message_code = array('message_code' => 403, 'message_dialog' => 'Email and Password are required');
                    $this->_sendAPI($message_code);
                }
            }
            else {
                $message_code = array('message_code' => 405, 'message_dialog' => 'Method Not Allowed');
                $this->_sendAPI($message_code);
            }
        }

        public function AddAction(){

            
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                //$params = $this->getRequest()->getParams();
                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);

                    
                    $customerFirstName = $params['firstname'];
                    $customerLastName = $params['lastname'];
                    $customerEmail = $params['email'];
                    $customerPassword = $params['password'];
                    $accountType = $params['account_type'];

                    
                    if (!empty($customerEmail) && !empty($customerPassword)){
                        $customer = Mage::getModel('customer/customer');
                        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
                        $customer->loadByEmail($customerEmail);

                        if(!$customer->getId()) {
                            $customer->setEmail($customerEmail);
                            $customer->setFirstname($customerFirstName);
                            $customer->setLastname($customerLastName);
                            $customer->setPassword($customerPassword);

                            try{
                                $customer->save();
                                //Session
                                $session = Mage::getSingleton('customer/session');
                                $session->start();
                                $session->setCustomerAsLoggedIn($customer);
                                $session->login($customerEmail, $customerPassword);
                                $encrypted = Mage::helper('core')->encrypt($session->getId());
                                
                                if($customer->getId() && $params['account_type']=='business'){
                                    $business_id=$this->_createBusinessProfile($customer->getId());
                                    if($business_id){
                                        $address_id=$this->_createAddressByBusinessId($business_id);
                                    }
                                    
                                }
                                
                                $apiarray = array(
                                    'message_code' => 200, 
                                    'message_dialog' => 'Register Success',
                                    'customer'=>array(
                                        'customer_id' =>$customer->getId(), 
                                        'account_type' =>$params['account_type'], 
                                        'business_id' =>$business_id , 
                                        'address_id' =>$address_id, 
                                        'token' => $encrypted, 
                                        'isLogin' => true, 
                                    )
                                    
                                    );
                                $this->_sendAPI($apiarray);     
                            }
                            catch (Exception $e) {
                                //Zend_Debug::dump($e->getMessage());
                                $message_code = array(
                                        'message_code' => 400,
                                        'message_dialog' => $e->getMessage());
                                $this->_sendAPI($message_code);
                            } 
                        }
                        else {
                            $message_code = array('message_code' => 400, 'message_dialog' => 'Email already exist in the system');
                            $this->_sendAPI($message_code);
                        }
                    }

                }
                else {
                    $message_code = array('message_code' => '405', 'message_dialog' => 'Method Not Allowed');
                    $this->_sendAPI($message_code);
                }
        }

        

        public function _createBusinessProfile($id){
            $business = Mage::getModel('Module/business');
            $business->setBusinessOwnerId($id);
            $business->save();
            $business_id=$business->getBusinessId();

            if($business_id){
                return $business_id;
            }
            else{
                return false;
            }
  
        }

        public function _getBusinessProfile($id){
            $business = Mage::getModel('Module/business');
            $business->load($id,'business_owner_id');
          
            $business_id=$business->getBusinessId();

            if($business_id){
                return $business_id;
            }
            else{
                return false;
            }
  
        }

        public function _createAddressByBusinessId($id){
            $address = Mage::getModel('Module/address');
            $address->setBusinessId($id);
            $address->save();
            $address_id=$address->getAddressId();

            if($address_id){
                return $address_id;
            }
            else{
                return false;
            }
  
        
        }

        public function ViewBusinessCustomerAction(){
            $params = $this->getRequest()->getParams();
            $business_id = $params['business_id'];
            $customer_id =  $params['customer_id'];

            if($business_id&&$customer_id){
                $response = array(
                    'message_code' => 200, 
                    'message_dialog' => 'Success',
                    'customer'=> $this->_getCustomerDetailsByCustomerId($business_id,$customer_id),
                );
            }
            else{
                $response = array(
                    'message_code' => 200, 
                    'message_dialog' => 'Success',
                    'customers'=> $this->_getCustomerListByBusinessId($business_id),
                );
            }


            
            $this->_sendAPI($response);  
        }

        public function AddBusinessCustomerAction(){


            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                
                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);

                try {
                    if($params['customer_id']){
                        $customer = Mage::getModel('Module/customer')->load($params['customer_id']);
                    }
                    else{
                        $customer = Mage::getModel('Module/customer');
                    }
                    $customer->setBusinessId($params['business_id']);
                    $customer->setFirstName($params['first_name']);
                    $customer->setLastName($params['last_name']);
                    $customer->setPhone($params['phone']);
                    $customer->setEmail($params['email']);
                    $customer->save();

                    $response = array( 
                        'status_code' => 200,
                        'message_dialog' => 'Success',
                        'customer' => array(
                            'customer_id' =>$customer->getId(),
                            'first_name' =>$customer->getFirstName(),
                            'last_name' =>$customer->getLastName(),
                            'phone' =>$customer->getPhone(),
                            'email' =>$customer->getEmail(),
                        )
                        
                    );

                    
                } catch (Exception $e) {
                    $response = array( 
                        'status_code' => 400,
                        'message_dialog' => $e,
                        
                        
                    );
                }
                $this->_sendAPI($response);
              
            }

        }

        private function _getCustomerDetailsByCustomerId($business_id,$customer_id){
            $customer = Mage::getModel('Module/customer')->load($customer_id);
          
            $response=array(
                'customer_id' =>$customer->getId(),
                'first_name' =>$customer->getFirstName(),
                'last_name' =>$customer->getLastName(),
                'phone' =>$customer->getPhone(),
                'email' =>$customer->getEmail(),
            );
            
            return $response;
        }

        private function _getCustomerListByBusinessId($business_id){
            
            try {
                $customers = Mage::getModel('Module/customer')
                ->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('business_id', $business_id );
               // ->setOrder('created_at', 'desc');
                foreach($customers as $customer){
                    
                    $response[] = array( 
                        'customer_id' =>$customer->getId(),
                        'first_name' =>$customer->getFirstName(),
                        'last_name' =>$customer->getLastName(),
                        'phone' =>$customer->getPhone(),
                        'email' =>$customer->getEmail(),

                    );
                }
                
                return $response;

                
            } catch (Exception $e) {
               
            }
           
            
            
            return $response;
        }

    }

?>
