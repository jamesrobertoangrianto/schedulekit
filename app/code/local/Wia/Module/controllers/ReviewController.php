<?php
    class Wia_Module_ReviewController extends Mage_Core_Controller_Front_Action
    {

        public function testAction(){
            $review = Mage::getModel('Module/review')->load(31);
            
        //     $review->setRating(1);
             
        //    $review->save();

           
            
            $this->_sendAPI($review);
        }

        public function addNotificationAction(){
            
            
            $review_id = $this->getRequest()->getParam('review_id');
            $business_id = $this->getRequest()->getParam('business_id');
            
            // $order = Mage::getModel('sales/order')->load($id);
            // $cutomer =  $this->_getCustomerById($order);
            // $product = $this->_getProducts($order);
            $business = $this->_getBusinessById($business_id);

            $response = array( 
                'status_code' => 200,
                'business_name'=>$business['name'],
                'business_address'=>$business['address']['street'],
                'business_region'=>$business['address']['region'],
                'business_city'=>$business['address']['city'],
                'business_phone'=>$business['address']['telephone'],

               'action_url'=>'https://getnotify.id/admin/review/'.$business_id.'/review_id/'.$review_id
                
            );

            $this->_sendMail(5,$response,$business['name'],$business['address']['email']);
            $this->_sendAPI($response);


        }
        
        public function _getBusinessById($id){
            
            $business = Mage::getModel('Module/business')->load($id);
            $response = array( 
                'id' => $business->getBusinessId(),
                //'business_owner_id' => $business->getBusinessOwnerId(),
                'name' => $business->getName(),
                'type' => $business->getType(),
                'category' => $business->getCategory(),
                'short_description' => $business->getShortDescription(),
                'description' => $business->getDescription(),
                //'social_media' => $this->_getSocialMedia($busines),

                //entitiy db
                'address' => $this->_getBusinessAddressById($business->getBusinessId()),
                //entitiy db
                //'photos' => $this->_getGalleryListByBusinessId($business->getBusinessId()),
                //entitiy db
                //'attribute' => $this->_getBusinessAttributeById($business->getBusinessId()),
               // 'customer' => $this->_getCustomerById($business->getBusinessOwnerId()),

            );
            return $response;
        }

        public function _getBusinessAddressById($business_id){
            
            $address = Mage::getModel('Module/address');
            $address->load($business_id,'business_id');
          
            $response = array( 

                'id' =>  $address->getAddressId(),
                'business_id' =>  $address->getBusinessId(),
                'street' => $address->getStreet(),
                'telephone' => $address->getTelephone(),
                'email' => $address->getEmail(),
                'city' => $address->getCity(),
                'region' => $address->getRegion(),
                'website' => $address->getWebsite(),
                'coordinates' => $address->getCoordinates(),
            );
          
            return $response;
        }



        public function _sendMail($templateId,$var,$recepientName,$recepientEmail){
            
            try {
                // Enter you new template ID
                $senderName = Mage::getStoreConfig('trans_email/ident_support/name');  //Get Sender Name from Store Email Addresses
                $senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');  //Get Sender Email Id from Store Email Addresses
                $sender = array('name' => $senderName,'email' => $senderEmail);
                
                $store = Mage::app()->getStore()->getId();
                
                Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, $recepientEmail, $recepientName, $var, $storeId);
                
            } catch (Mage_Core_Exception $e) {
                //throw $th;
            }
                
        }

        
        public function _sendAPI($data){
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
        }
        
        public function addAction(){

            $getraws = $this->getRequest()->getRawBody();
            $params = json_decode($getraws, true);
            $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
            if($params['review_id'] == 'new' ){
                $review = Mage::getModel('Module/review');
                $review->setStatus(0);
                $review->setRating($params['rating']);
                $review->setCustomerName($params['customer_name']);
                $review->setDescription($params['description']);
                $review->setBusinessId($params['business_id']);
                $review->setTimestamp($date);
                $review->save();
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                    'review' => $review->toArray(),
                );
            }
            else{
                $review = Mage::getModel('Module/review')->load($params['review_id']);
                if($review){
                    $review->setStatus(1);
                    $review->setResponse($params['response']);
                    $review->save();
                    $response = array( 
                        'status_code' => 200,
                        'message_dialog' => 'Success',
                        'review' => $review->toArray(),
                    );
                }
                else{
                    $response = array( 
                        'status_code' => 400,
                        'message_dialog' => 'Success',
                       
                    );
                }
            }
           
            

            
            $this->_sendAPI( $response );

        }
        public function viewAction(){
            $params = $this->getRequest()->getParams();
            $business_id =  $params['business_id'];
            $review_id =  $params['review_id'];
            $status =  $params['status'];

            if($business_id){
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                    'business_id'=> $business_id,
                    'review' => $this->_getReviewsByBusinessId($business_id,$status),
                );
            }
            if($review_id){
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                    'review_id'=> $review_id,
                    'review' => $this->_getReviewReviewId($review_id),
                );
            }
           
            
            $this->_sendAPI($response);
             
        }

        public function _getReviewsByBusinessId($business_id,$status){
           
            $reviews = Mage::getModel('Module/review')
                ->getCollection()
                ->addFieldToSelect('*')
                ->setOrder('review_id')
                ->addFieldToFilter('business_id', $business_id );
                if($status){
                    $reviews->addFieldToFilter('status', $status );
                }
                
        
                foreach( $reviews as $review){
                    $total_rating[] = $review->getRating();
                    $items[] = array( 
                        'review_id' => $review->getReviewId(),
                        'business_id' => $review->getBusinessId(),
                        'rating' => $review->getRating(),
                        'title' => $review->getTitle(),
                        'description' => $review->getDescription(),
                        'response' => $review->getResponse(),
                        'verified' => $review->getVerified(),
                        'status' => $review->getStatus(),
                        'time' => $review->getTimestamp(),
                        'customer_name'=> $review->getCustomerName(),
                        );
                   
                }
                $response = array( 
                    'total_review' => $reviews->getSize(),
                    'total_rating' =>  array_sum($total_rating)?array_sum($total_rating)/$reviews->getSize():0,
                    'items' => $items,
                    );
                    return $response;
                
        }
        public function _getReviewReviewId($review_id){
            $review = Mage::getModel('Module/review')->load($review_id);
            if($review){
                $response = array( 
                    'review_id' => $review->getReviewId(),
                    'business_id' => $review->getBusinessId(),
                    'rating' => $review->getRating(),
                    'title' => $review->getTitle(),
                    'description' => $review->getDescription(),
                    'response' => $review->getResponse(),
                    'verified' => $review->getVerified(),
                    'status' => $review->getStatus(),
                    'time' => $review->getTimestamp(),
                    'customer_name'=> $review->getCustomerName(),
                    );
                return $response;
            }
            

        }
        public function editAction(){
            
        }
        public function deleteAction(){
            
        }


        
       
    }

?>
