<?php
    class Wia_Module_ReportsController extends Mage_Core_Controller_Front_Action
    {

        public function _sendAPI($data){
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
        }
    
        public function _validate(){
            return true;
        }

        public function testaction(){
            $gallery = Mage::getModel('Module/customer');
                    $gallery->setBusinessId(1);
                    $gallery->setFirstName('dada');
                    $gallery->save();

            echo $business_id;
        }
        public function viewSalesAction(){
            $params = $this->getRequest()->getParams();
            $status = $params['status'];
            $business_id = $params['business_id'];
            $orders = Mage::getResourceModel('sales/order_collection')
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('business_id', $business_id  )
                    ->addFieldToFilter('status', $status)
                    //->addAttributeToSelect( 'grand_total' )
                    ->setOrder('created_at', 'desc');

            foreach($orders as $order){
                $items []= array( 
                    'grand_total' => $order->getGrandTotal()
              
                );
            }
            
           // $totalSum = array_sum( $items['item']);  

            $response = array( 
                'status_code' => 200,
                'message_dialog' => 'Success',
                'endpoint'=> 'sales',
                'status' => $status,
                'business_id' => $business_id,
                'total_order' => $orders->getSize(),
                'total_transaction' => array_sum(array_column($items, 'grand_total')),
            );
            $this->_sendAPI($response);
            
        }
        public function viewCustomerAction(){
            $params = $this->getRequest()->getParams();
            $status = $params['status'];
            $business_id = $params['business_id'];
            
            $customers = Mage::getModel('Module/customer')
            ->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('business_id', $business_id );
          
            $response = array( 
                'status_code' => 200,
                'message_dialog' => 'Success',
                'status' => $status,
                'endpoint'=> 'customer',
                'business_id' => $business_id,
                'total_customer' => $customers->getSize(),
            );
            
            $this->_sendAPI( $response );
            
        }
        public function viewProductAction(){
            $params = $this->getRequest()->getParams();
            $status = $params['status'];
            $business_id = $params['business_id'];
            $type='simple';
           
            $products = Mage::getModel("catalog/product")
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status', $status)
            ->addAttributeToFilter('type_id', $type)
            ->addAttributeToFilter('business_id', $business_id);
            


            $response = array( 
                'status_code' => 200,
                'message_dialog' => 'Success',
                'status' => $status,
                'endpoint'=> 'product',
                'business_id' => $business_id,
                'total_product' => $products->getSize(),
               
                
            );
            $this->_sendAPI( $response );
            
        }

        
 
    }

?>
