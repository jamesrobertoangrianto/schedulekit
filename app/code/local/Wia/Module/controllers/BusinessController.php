<?php
    class Wia_Module_BusinessController extends Mage_Core_Controller_Front_Action
    {

        public function testAction(){
            $business = Mage::getModel('Module/business')->load(1);
            $this->_sendAPI($business );
        }
        public function _sendAPI($data){
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
        }
    
        public function _validate(){
            return true;
        }
    

        public function _getBusinessById($id){
            
            $business = Mage::getModel('Module/business')->load($id);
            $response = array( 
                'id' => $business->getBusinessId(),
                'business_owner_id' => $business->getBusinessOwnerId(),
                'name' => $business->getName(),
                'type' => $business->getType(),
                'category' => $business->getCategory(),
                
                'short_description' => $business->getShortDescription(),
                'description' => $business->getDescription(),
                'social_media' => $this->_getSocialMedia($busines),

                //entitiy db
                'address' => $this->_getBusinessAddressById($business->getBusinessId()),
                //entitiy db
                'photos' => $this->_getGalleryListByBusinessId($business->getBusinessId()),
                //entitiy db
                'cover_image'=> $business->getCoverImage(),
                'profile_image'=> $business->getProfileImage(),

                'attribute' => $this->_getBusinessAttributeById($business->getBusinessId()),
                'customer' => $this->_getCustomerById($business->getBusinessOwnerId()),

            );
            return $response;
        }

        public function _getCustomerById($customerId){
            $customer = Mage::getModel('customer/customer')->load($customerId);
            $response = array( 
                'name' => $customer->getName(),
                'photo_url' => $customer->getPhotoUrl(),
                'last_active' => 'today',
            );
            return $response;
        }

        public function _getBusinessAttributeById(){
            for ($x = 0; $x <= 3; $x++) {
                $response[] = array( 

                    'id' => $x,
                    'attribute_name' => 'Cozy Studio'
                );
            }
          
            return $response;
        }

        public function _getBusinessAddressById($business_id){
            
            $address = Mage::getModel('Module/address');
            $address->load($business_id,'business_id');
          
            $response = array( 

                'id' =>  $address->getAddressId(),
                'business_id' =>  $address->getBusinessId(),
                'street' => $address->getStreet(),
                'telephone' => $address->getTelephone(),
                'email' => $address->getEmail(),
                'city' => $address->getCity(),
                'region' => $address->getRegion(),
                'website' => $address->getWebsite(),
                'coordinates' => $address->getCoordinates(),
            );
          
            return $response;
        }

        public function _getSocialMedia($busines){
            $response = array( 
                'instagram' => array(
                    'url' => 'instagram_url',
                    'name' => 'instagram_name',
                ),
              
               
               
            );
          
            return $response;
        }


        public function _getBusinessPhotosById($id){
           
            for ($x = 0; $x <= 3; $x++) {
                $response[] = array( 

                    'photo_url' => 'https://images.unsplash.com/photo-1518310952931-b1de897abd40?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&h=500&q=80',
                      
                );
            }
           
            return $response;
        }


        public function viewAction(){
            $this->_validate();
            $id = $this->getRequest()->getParam('id');
           
            if($id){
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                    'business' => $this->_getBusinessById($id),
                );
                
            }
            else{
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Message and Recepient are required'
                );
            }

            $this->_sendAPI( $response);
            
            
        }

        public function addAction(){
            $params = $this->getRequest()->getParams();

            try {
                $business = Mage::getModel('Module/business');
                $business->setBusinessOwnerId($params['business_owner_id']);
                $business->save();
                $business_id=$business->getBusinessId();

                if($business_id){
                    //generate new address address
                    $address = Mage::getModel('Module/address');
                    $address->setBusinessId($business_id);
                    $address->save();
                    $address_Id = $address->getAddressId();
                }
                
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                    'business' => array(
                        id => $business_id,
                        address_id => $address_Id,
                        business_owner_id => $business->getBusinessOwnerId()
                    )
                    
                );




            } catch (Exception $e) {
                $response = array( 
                    'status_code' => 400,
                    'message_dialog' => $e,
                    
                    
                );
            }

            

            $this->_sendAPI($response);
            
        }

        public function editAction(){
            //$params = $this->getRequest()->getParams();
            $getraws = $this->getRequest()->getRawBody();
            $params = json_decode($getraws, true);

            
            $business = Mage::getModel('Module/business')->load($params['id']);
            $business->setName($params['name'])
                ->setType($params['type'])
                //->setCategoryId($params['category_id'])
                //->setWebsite($params['website'])
                ->setShortDescription($params['short_description'])
                ->setDescription($params['description'])
                ->setAttribute($params['attribute']);
            
            $business->save();
            $response = array( 
                'status_code' => 200,
                'message_dialog' => 'Success',
                'business' =>  $business->toArray(),
                
            );

            $this->_sendAPI( $response);
            
            
        }

        public function editAddressAction(){
           // $params = $this->getRequest()->getParams();
            $getraws = $this->getRequest()->getRawBody();
            $params = json_decode($getraws, true);

            
            $address = Mage::getModel('Module/address')->load($params['id']);
            
            if($address->getBusinessId() == $params['business_id']){
                $address->setCity($params['city'])
                    ->setTelephone($params['telephone'])
                    ->setEmail($params['email'])
                    ->setStreet($params['street'])
                    ->setRegion($params['region'])
                    ->setWebsite($params['website'])
                    ->setCoordinates($params['coordinates']);
                $address->save();
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                    'address' => $address->toArray()           
                );
            }
            
            else{
                $response = array( 
                    'status_code' => 400,
                    'message_dialog' => 'not_allowed',
                    
                );
            }
            

            $this->_sendAPI( $response);
            
            
        }

        public function deleteAction(){
            $id = $this->getRequest()->getParam('id');
            echo '123';

            $this->_sendAPI( $response);
            
            
        }

        //business hours
        public function viewBusinessHoursAction(){
            $id = $this->getRequest()->getParam('business_id');
            $business_hours = Mage::getModel('Module/businesshours')
                ->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('business_id',$id);
           
            if($business_hours->getSize()==0){
                $business_hours = $this->_addBusinessHours($id);
            }
            
            

            foreach($business_hours as $business_hour){
                if($business_hour->getStatus()==1){
                    $status = true;
                }
                else{
                    $status = false;
                }
                $data[] = array( 
                    'business_hours_id'=>$business_hour->getBusinessHoursId(),
                    'day' => $business_hour->getDay(),
                    'status' => $status,
                    'start_time' => $business_hour->getStartTime(),
                    'end_time' =>$business_hour->getEndTime(),
                );
            }

            $response = array( 
                'status_code' => 200,
                'message_dialog' => 'Success',
                'business_hours' => $data,
                
            );
            $this->_sendAPI($response);
        }

        public function editBusinessHoursAction(){
            $id = $this->getRequest()->getParam('business_id');
            $getraws = $this->getRequest()->getRawBody();
            $params = json_decode($getraws, true);
            

            if($params['status']==true){
                $status = 1;
            }
            else{
                $status = 0;
            }

            $business_hours = Mage::getModel('Module/businesshours')->load($params['business_hours_id']);
            $business_hours->setBusinessId($id);
            $business_hours->setStartTime($params['start_time']);
            $business_hours->setEndTime($params['end_time']);
            $business_hours->setStatus($status);
            $business_hours->save();
            
            if($business_hours->getBusinessHoursId()){
                $response = array(
                    'business_id' => $id,
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                );
            }
            
            $this->_sendAPI($response);
        }

        public function removeBusinessHoursAction(){
            $business_hours = Mage::getModel('Module/businesshours')->load(3);
            // $business_hours->setStatus(0);
            $business_hours->delete();
            $this->_sendAPI($business_hours->getStatus());
        }

        

        public function _addBusinessHours($id){
           
            for ($x = 0; $x <= 6; $x++) {
                $business_hours = Mage::getModel('Module/businesshours');
                $business_hours->setBusinessId($id);
                $business_hours->setDay($x);
                $business_hours->setStatus(1);
                $business_hours->save();
            }
            
            return $business_hours;
        }

       

        //business gallery

        public function addGalleryAction(){
            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                
                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);

            if($params['business_id']){
                try {
                    $gallery = Mage::getModel('Module/gallery');
                    $gallery->setBusinessId($params['business_id']);
                    $gallery->setImagePath($this->_getImagePath($params));
                   
                    $gallery->save();
                    $response = array( 
                        'status_code' => 200,
                        'message_dialog' => 'Success',
                        'gallery' => array(
                            gallery_id => $gallery->getGalleryId(),
                            business_id => $gallery->getBusinessId(),
                            image_path => $gallery->getImagePath(),
                            image_name => '',
                        )
                        
                    );

                    
                } catch (Exception $e) {
                    $response = array( 
                        'status_code' => 400,
                        'message_dialog' => $e,
                        
                        
                    );
                }
                $this->_sendAPI($response);
    
            }
            else{
                $response = array( 
                    'status_code' => 400,
                    'message_dialog' => 'not allowed',
                    
                    
                );
            }
            $this->_sendAPI($response);
            }

        }

        public function editGalleryAction(){
            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                $params = $this->getRequest()->getParams();
                $business_id= $params['business_id'];
                $id= $params['gallery_id'];
                $action= $params['action'];

                $gallery = Mage::getModel('Module/gallery')->load($id);
                $business = Mage::getModel('Module/business')->load($business_id);
                $business->setData($action,$gallery->getImagePath());
                $business->save();
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => ' allowed',
                    'data' => $params
                    
                );

                $this->_sendAPI($response);
            }
            

        }

        public function _getImagePath($params){
            
            $name = $params['image'];
            $base64 = $params['base64'];

            $first = substr($name, 0, 1);
            $second = substr($name, 1, 1);

            $file = $base64;
            $bin = base64_decode($base64);

            // Load GD resource from binary data
            $image = imageCreateFromString($bin);

            if (!$image) {
                $message_code = array('message_code' => 403, 'message_dialog' => 'Base64 value is not a valid image');
                $this->_sendAPI($message_code);
            }
            else {
                $path = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'promotion' . DS . $first . DS . $second;

                // Or you can also check if the directory does not exist right before creating it
                $io = new Varien_Io_File();
                if (!$io->fileExists($path, false)) {
                    $io->mkdir($path);
                }
                $newpath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'promotion' . DS . $first . DS . $second . DS . $name .'.png';
                file_put_contents($newpath, $bin);
                $image_path = 'https://api.getnotify.id/media'. DS . 'promotion' . DS . $first . DS . $second . DS . $name .'.png';
                return $image_path;
                
            }
                      
        }

        public function removeGalleryAction(){
            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
               
                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);
                $business_id=$params['business_id'];
                $gallery_id=$params['gallery_id'];
            
                if($params['business_id']){
                    try {
                        $gallery = Mage::getModel('Module/gallery')->load($params['gallery_id']);
                        if($gallery->getBusinessId()==$params['business_id']){
                            $gallery->delete();
                            $message='success';
                        }
                        else{
                            $message='not allowed';
                        }
                        

                        $response = array( 
                            'status_code' => 200,
                            'message_dialog' => $message,
                            'gallery' => array(
                                gallery_id => $gallery->getGalleryId(),
                                business_id => $gallery->getBusinessId(),
                            
                            )
                            
                        );

                        
                    } catch (Exception $e) {
                        $response = array( 
                            'status_code' => 400,
                            'message_dialog' => $e,
                            
                            
                        );
                    }
                    
        
                }
                else{
                    $response = array( 
                        'status_code' => 400,
                        'message_dialog' => 'not allowed',
                        
                        
                    );
                }
                $this->_sendAPI($response);
            }
   
        }

        public function viewGalleryAction(){
            $params = $this->getRequest()->getParams();
            $business_id=$params['business_id'];
            $gallery_id=$params['gallery_id'];
            
            
            if($business_id){
            
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'success',
                    'gallery'=>$this->_getGalleryListByBusinessId($business_id),
                    
                    
                );
               

               
            }
            
            if($business_id&&$gallery_id){
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'success',
                    'gallery'=> $this->_getGalleryById($gallery_id),
                    
                    
                );
               
            }
            $this->_sendAPI($response);
           
           
            
           
        }

        public function _getGalleryListByBusinessId($business_id){
            try {
                $gallerys = Mage::getModel('Module/gallery')
                ->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('business_id', $business_id  );
               // ->setOrder('created_at', 'desc');
                foreach($gallerys as $gallery){
                    $response[] = array( 
                        'gallery_id'=>$gallery->getGalleryId(),
                        'business_id'=>$gallery->getBusinessId(),
                        'image_path'=>$gallery->getImagePath(),
                        'image_name'=>$gallery->getImageName(),
                        

                    );
                }
                
                return $response;

                
            } catch (Exception $e) {
               
            }
            $this->_sendAPI($response);
        }

        public function _getGalleryById($id){
            try {
                $gallery = Mage::getModel('Module/gallery')->load($id);
               
                $response = array( 
                    'gallery_id'=>$gallery->getGalleryId(),
                    'business_id'=>$gallery->getBusinessId(),
                    'image_path'=>$gallery->getImagePath(),
                    'image_name'=>$gallery->getImageName(),
                    
                );
                return $response;

                
            } catch (Exception $e) {
                
            }
            
        }

        
       
    }

?>
