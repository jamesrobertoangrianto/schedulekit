<?php
    class Wia_Module_CatalogController extends Mage_Core_Controller_Front_Action
    {

        public function indexAction(){
            $posts = Mage::getResourceModel('wordpress/post_collection');
            $posts->addPostTypeFilter('inspiration');
                  

            $this->_sendAPI($posts);
   

        }
        public function _sendAPI($data){
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));

        }

        public function getProductByUrlAction(){
            $path = $this->getRequest()->getParam('url').'/';
            
            
            $store = Mage::app()->getStore();
            $rewrite = Mage::getModel('core/url_rewrite')
            ->setStoreId($store->getId())
            ->loadByRequestPath($path);


            // $category_id = $rewrite->getCategoryId();
            $product_id = $rewrite->getProductId();

            // echo $product_id;
            // echo $category_id;
            $product = Mage::getModel('catalog/product')->load($product_id);
            $this->_sendAPI($product);
         
        }

        public function getCategoryByUrlAction(){
            $path = $this->getRequest()->getParam('url').'/';
            
            
            
            $store = Mage::app()->getStore();
            $rewrite = Mage::getModel('core/url_rewrite')
            ->setStoreId($store->getId())
            ->loadByRequestPath($path);


            $category_id = $rewrite->getCategoryId();
            
            $products = Mage::getModel('catalog/category')->load($category_id)
            ->getProductCollection()
            ->addAttributeToSelect('*') // add all attributes - optional
            ->addAttributeToFilter('status', 1) // enabled
            ->addAttributeToFilter('visibility', 4) //visibility in catalog,search
            ->setOrder('price', 'ASC'); //sets the order by price

            foreach ($products as $product){
                $response[] = array( 
                    'id' => $product->getId(),
                    'name' => $product->getName(),
                    'is_in_stock' => $product->getFabric(),
                   
                    
                    
                   
                );
            }

           $this->_sendAPI($response);

            //var_dump($products);
         
        }

        

        public function getFeaturedJournalAction(){
            
            $posts = Mage::getResourceModel('wordpress/post_collection')
            ->addPostTypeFilter('post')
            ->setOrderByPostDate()
            ->addIsViewableFilter()
            ->setPageSize(3)
            ->load();

            foreach($posts  as $post){
                $response[] = array( 
                    'author' => $post->getAuthor()->getDisplayName(),
                    'date' => $post->getPostDate(),
                    'title' => $post->getPostTitle(),
                    
                    'url' => $post->getPermalink(),
                    'image' => $post->getFeaturedImage()->getThumbnailImage()
                    
                   
                );
            }
            
            
             $this->_sendAPI($response);
   
            
        }

        public function getChildCategoryByIdAction(){
            $category_id = $this->getRequest()->getParam('id');       
            $parrent_category = Mage::getModel('catalog/category')->load($category_id);
            $childCategorys = $parrent_category->getChildrenCategories();

            foreach($childCategorys as $childCategory){
                $category = Mage::getModel('catalog/category')->load($childCategory->getId());
                $response[] = array( 
                    'name' => $category->getName(),
                    'label' => $category->getData('caategory_label'),
                    'title' => $category->getData('category_title'),
                    'description' => $category->getCategoryShortDescription(),
                    'url' => $category->getUrl(),
                    'promo' => $category->getData('caategory_promo'),
                    'image' => $category->getResizedImage(200,250,78),
                    'image_2x' => $category->getResizedImage(400,500,78),
                    'image_3x' => $category->getResizedImage(800,1000,78),
                    'image_4x' => $category->getResizedImage(1600,2000,78),
                   
                );
            }
            
            
             $this->_sendAPI($response);
            
        }


        public function getCategoryByIdsAction(){
            $category_id = $this->getRequest()->getParam('id');  
            $terms = explode(',',  $category_id);   
            
            foreach($terms as $term){
                $category = Mage::getModel('catalog/category')->load($term);
                $name = $category->getData('category_title');

                $response[] = array( 
                    'name' => $category->getName(),
                    'label' => $category->getData('caategory_label'),
                    'title' => $category->getData('category_title'),
                    'description' => mb_strimwidth($name, 0, 40, "..."),
                    'url' => $category->getUrl(),
                    'promo' => $category->getData('caategory_promo'),
                    'image' => $category->getResizedThumbnailImage(175,125,78),
                    'image_2x' => $category->getResizedThumbnailImage(350,250,78),
                    'image_3x' => $category->getResizedThumbnailImage(700,500,78),
                    
                   
                );
            }


            
            
            
            $this->_sendAPI($response);
            
        }

        public function getFeaturedPromoAction(){
            
            $layout = $this->getLayout();
            $shippingaddressBlock = $layout->createBlock('checkout/cart_totals')
            ->setData('id','brand')
            ->setTemplate('catalog/featuredblock/card/promo.phtml');
            echo $shippingaddressBlock->toHtml();
            
        }

       




    }

?>
