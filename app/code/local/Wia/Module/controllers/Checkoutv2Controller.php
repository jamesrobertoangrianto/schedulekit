<?php
class Wia_Module_Checkoutv2Controller extends Mage_Core_Controller_Front_Action{

    public function _sendAPI($data){
        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));

       
    }
    

    public function _getCustomer(){
        // $customer = Mage::getModel('customer/customer')->load(18);
        // $customer = Mage::getModel('customer/customer')->load(1);
        $customer= Mage::getSingleton('customer/session')->getCustomer();

        return $customer;
    }

    protected function _clearsession(){
        // Clear Magento cart and quote items
        $cart = Mage::getModel('checkout/cart');                
        $cart->truncate()->save(); // remove all active items in cart page
        $cart->init();
        $session= Mage::getSingleton('checkout/session');
        $quote = $session->getQuote();
        $cart = Mage::getModel('checkout/cart');
        $cartItems = $cart->getItems();
        foreach ($cartItems as $item)
        {
            $quote->removeItem($item->getId())->save();
        }
        Mage::getSingleton('checkout/cart')->truncate();
        Mage::getSingleton('checkout/cart')->save();
        Mage::getSingleton('checkout/session')->clear();
                    
    }

    public function _getImagePath($data){
        $image = $data;
        return Mage::getModel('catalog/product_media_config')->getMediaUrl($image);
    }

    public function _setAddressToCart($data){
        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        $customerShippingAddress = $data;
        // Update the cart's quote.
        $cart = Mage::getSingleton('checkout/cart');
        $address = $cart->getQuote()->getShippingAddress();

        $address->setCountryId($customerShippingAddress->getCountryId())
                ->setPostcode($customerShippingAddress->getPostcode())
                ->setCity($customerShippingAddress->getCity())
                ->setRegionId($customerShippingAddress->getRegionId())
                ->setRegion($customerShippingAddress->getregion())
                ->setStreet(array(
                    '0' => $customerShippingAddress->getStreet(1), // compulsory
                    '1' => $customerShippingAddress->getStreet(2) // optional
                ))
                ->setTelephone($customerShippingAddress->getTelephone())
                ->setData('vat_id', $customerShippingAddress->getData('vat_id'))
                ->setCollectShippingrates(true);
        $address->save();
        $cart->save();
    }

    public function resizeImage($fullPath, $fileName, $width, $height, $quality){
        // $folderURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $imageURL = $fullPath;

        $category = 0;

        $ProductPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'product' . $fileName;
        $CategoryPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'category' . DS . $fileName;
        $ProfilePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS  . 'host' . $fileName;

        $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'cache' . DS . $width . 'x' . $height . $fileName;
        $newCategoryPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'cache' . DS . $width . 'x' . $height . DS . $fileName;
        $newProfilePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'host' . DS . 'cache' . DS . $width . 'x' . $height . DS . $fileName;
        //if width empty then return original size image's URL

            if ($width != '') {
                //if image has already resized then just return URL
                if (file_exists($CategoryPath) && is_file($CategoryPath) && !file_exists($newCategoryPath)) { 
                    $imageObj = new Varien_Image($CategoryPath);
                    $imageObj->constrainOnly(TRUE);
                    $imageObj->keepAspectRatio(FALSE);
                    $imageObj->keepFrame(FALSE);
                    $imageObj->quality ( $quality );
                    $imageObj->backgroundColor(array(255,255,255));
                    $imageObj->resize($width, $height);
                    $imageObj->save($newCategoryPath);
                    // imagewebp(imagecreatefrompng($read_from_path), $save_to_path + ".webp");

                    $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'cache' . DS . $width . 'x' . $height . DS . $fileName;
                }
                else if(file_exists($ProductPath) && is_file($ProductPath) && !file_exists($newPath)){
                    $imageObj = new Varien_Image($ProductPath);
                    $imageObj->constrainOnly(TRUE);
                    $imageObj->keepAspectRatio(FALSE);
                    $imageObj->keepFrame(FALSE);
                    $imageObj->quality ( $quality );
                    $imageObj->backgroundColor(array(255,255,255));
                    $imageObj->resize($width, $height);
                    $imageObj->save($newPath);

                    $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'cache' . DS . $width . 'x' . $height . $fileName;
                }
                else if(file_exists($ProfilePath) && is_file($ProfilePath) && !file_exists($newProfilePath)){
                    $imageObj = new Varien_Image($ProfilePath);
                    $imageObj->constrainOnly(TRUE);
                    $imageObj->keepAspectRatio(FALSE);
                    $imageObj->keepFrame(FALSE);
                    $imageObj->quality ( $quality );
                    $imageObj->backgroundColor(array(255,255,255));
                    $imageObj->resize($width, $height);
                    $imageObj->save($newProfilePath);

                    $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'host' . DS . 'cache' . DS . $width . 'x' . $height . DS . $fileName;
                }
                else if(file_exists($newCategoryPath)){
                    $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'cache' . DS . $width . 'x' . $height . DS . $fileName;
                }
                else if(file_exists($newProfilePath)){
                    $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'host' . DS . 'cache' . DS . $width . 'x' . $height . DS . $fileName;
                }
                else {
                    $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'cache' . DS . $width . 'x' . $height . $fileName;
                }
            } 
            else {
                $resizedURL = $imageURL;
            }
        // return imagewebp($resizedURL, 'php.webp');
        return $resizedURL;
    }

    public function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();
       
        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    public function UserValidationAction(){
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();

            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'User is Logged in'
            );
        }
        else {
            $message_code = array(
                'message_code' => 400,
                'message_dialog' => 'User is not Logged in / session expired'
            );
        }
        $this->_sendAPI($message_code);
    }

    public function CheckoutValidationAction(){

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        $quote = Mage::helper('checkout/cart')->getCart()->getQuote();

        foreach ($quote->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            $product = Mage::getModel("catalog/product")->load($item->getProductId());

            if($product->getStockItem()->getQty() > $item->getQty()){
                $value[]= array (
                    'id' => $item->getProductId(),
                    'name' => $item->getName(),
                    'brand' => $product->getAttributeText("brand"),
                    'quantity' => $item->getQty(),
                    'product_quantity' => $product->getStockItem()->getQty() * 1,
                    'price' => $item->getParentItemId()?  $item->getParentItem()->getPrice(): $item->getPrice()
                );
            }
            else {
                $error[]= array (
                    'id' => $item->getProductId(),
                    'name' => $item->getName(),
                    'brand' => $product->getAttributeText("brand"),
                    'quantity' => $item->getQty(),
                    'product_quantity' => $product->getStockItem()->getQty() * 1,
                    'price' => $item->getParentItemId()?  $item->getParentItem()->getPrice(): $item->getPrice()
                );
            }
        }
        $status_true = array(
            'message_code' => 200,
            'message_dialog' => 'This product can be bought',
            'product' => $value
        );
        $status_false = array(
            'message_code' => 400,
            'message_dialog' => 'This product cannot be bought',
            'product' => $error
        );
        $message_code = array(
            'true' => $status_true,
            'error' => $status_false
        );
        $this->_sendAPI($message_code);
    }

    public function AddressAction(){
        // $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customer = $this->_getCustomer();

        $params = $this->getRequest()->getParams();

        $addressId = $params['id'];
        $show = $params['show'];

        if ($_SERVER['REQUEST_METHOD'] === 'GET'){
            if($show == 1){
                $this->_getAddress($customer);
            }
            else {
                $this->_getAddressList($customer);
            }
        }
        else if ($_SERVER['REQUEST_METHOD'] === 'POST'){
            if(!empty($addressId)){
                $this->_setDefault($customer, $addressId);
            }
            else {
                $this->_createNewAddress($customer);
            }
        }
        else if ($_SERVER['REQUEST_METHOD'] === 'PUT'){
            $this->_editAddressbyAddressId($customer, $addressId);
        }
        else {
            $message_code = array('message_code' => 405, 'message_dialog' => 'Method Not Allowed');
            $this->_sendAPI($message_code);
        }
    }

    public function _getAddressList($data){
        $customer = $data;
        
        foreach ($customer->getAddresses() as $address){
        //$data = $address->toArray();
            $customeraddress[] = array(
                'id' => $address->getId(),
                'customer_id' => $customer->getId(),
                'firstname' => $customer->getFirstname(),
                'lastname' => $customer->getLastname(),
                'email' => $customer->getEmail(),
                'company' => $address->getCompany(),
                'city' => $address->getCity(),
                'region' => $address->getRegion(),
                'postcode' => $address->getPostcode(),
                'country_id' => $address->getCountryId(),
                'telephone' => $address->getTelephone(),
                'region_id' => $address->getRegionId(),
                'street' => $address->getStreet(1),
                'location' => $address->getStreet(2),
                'vat_id' => $address->getData('vat_id')
            );
        }

        if(!empty($customeraddress)){
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Get Address List success',
                'address_list' => $customeraddress
            );
        }
        else {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Address Not Found',
                'address_list' => $customeraddress
            );
        }

        $this->_sendAPI($message_code);
    }

    public function _getAddress($data){
        $customer = $data;
        $address = $customer->getPrimaryShippingAddress();

        if(!empty($address)){
            $customeraddress = array(
                'id' => $address->getId(),
                'customer_id' => $customer->getId(),
                'firstname' => $customer->getFirstname(),
                'lastname' => $customer->getLastname(),
                'email' => $customer->getEmail(),
                'company' => $address->getCompany(),
                'city' => $address->getCity(),
                'region' => $address->getRegion(),
                'postcode' => $address->getPostcode(),
                'country_id' => $address->getCountryId(),
                'telephone' => $address->getTelephone(),
                'region_id' => $address->getRegionId(),
                'street' => $address->getStreet(1),
                'location' => $address->getStreet(2),
                'vat_id' => $address->getData('vat_id')
            );
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Get Address Success',
                'address' => $customeraddress
            );
        }
        else {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Address not found',
                'address' => $customeraddress
            );
        }

        $this->_sendAPI($message_code);
    }

    public function _editAddressbyAddressId($data, $data1){
        $customer = $data;
        $addressId = $data1;
        // $address = $customer->getPrimaryShippingAddress();
        $address = Mage::getModel("customer/address")->load($addressId);
    
        $getraws = $this->getRequest()->getRawBody();
        $params = json_decode($getraws, true);

        // $customerFirstname = $params['firstname'];
        // $customerLastname = $params['lastname'];
        $customerCompany = $params['company'];
        $customerTelephone = $params['telephone'];
        $customerCity = $params['city'];
        $customerRegion = $params['region'];
        $customerRegionId = $params['region_id'];
        $customerPostalcode = $params['postcode'];
        $customerCountryId = $params['country_id'];   
        $customerStreet = $params['street'];
        $location = $params['location'];
        $vat = $params['vat_id']; 

        $street = array(
            '0' => $customerStreet, // compulsory
            '1' => $location // optional
        );

        $address->setCountryId($customerCountryId);
        $address->setPostcode($customerPostalcode);
        $address->setCity($customerCity);
        $address->setTelephone($customerTelephone);
        $address->setCompany($customerCompany);
        $address->setStreet($street);
        $address->setRegion($customerRegion);
        $address->setRegionId($customerRegionId);
        $address->setData('vat_id', $vat);
        // $address->setData();
        // $address->setStreet(1, $customerStreet);
        // $address->setStreet(2, $location);
        $address->save();

        $this->_setAddressToCart($address);

        $customeraddress = array(
            'id' => $address->getId(),
            'customer_id' => $customer->getId(),
            'firstname' => $customer->getFirstname(),
            'lastname' => $customer->getLastname(),
            'email' => $customer->getEmail(),
            'company' => $address->getCompany(),
            'city' => $address->getCity(),
            'region' => $address->getRegion(),
            'postcode' => $address->getPostcode(),
            'country_id' => $address->getCountryId(),
            'telephone' => $address->getTelephone(),
            'region_id' => $address->getRegionId(),
            'street' => $address->getStreet(1),
            'location' => $address->getStreet(2),
            'vat_id' => $address->getData('vat_id')
        );

        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Address Edited',
            'address' => $customeraddress
        );

        $this->_sendAPI($message_code);
    }

    public function _setDefault($data, $data1){
        $customer = $data;
        $addressId = $data1;
        // $address = $customer->getPrimaryShippingAddress();
        $address = Mage::getModel("customer/address")->load($addressId);

        $address->setIsDefaultBilling('1');
        $address->setIsDefaultShipping('1');
        $address->save();

        $this->_setAddressToCart($address);

        $cart = Mage::getSingleton('checkout/cart');
        $address = $cart->getQuote()->getShippingAddress();

        $customeraddress = array(
            'id' => $address->getId(),
            'customer_id' => $customer->getId(),
            'firstname' => $customer->getFirstname(),
            'lastname' => $customer->getLastname(),
            'email' => $customer->getEmail(),
            'company' => $address->getCompany(),
            'city' => $address->getCity(),
            'region' => $address->getRegion(),
            'postcode' => $address->getPostcode(),
            'country_id' => $address->getCountryId(),
            'telephone' => $address->getTelephone(),
            'region_id' => $address->getRegionId(),
            'street' => $address->getStreet(1),
            'location' => $address->getStreet(2),
            'vat_id' => $address->getData('vat_id')
        );
        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Address has been set to default.',
            'address' => $customeraddress
        );

        $this->_sendAPI($message_code);
    }
    
    public function _createNewAddress($data){
            
        // if (!$this->_validateFormKey()) {
        //     $this->_redirect('*/*');
        //     return;
        // }

            //login customer data
            $websiteId = Mage::app()->getWebsite()->getId();
            $store = Mage::app()->getStore();
            $customer = $data;
        
            $getraws = $this->getRequest()->getRawBody();
            $params = json_decode($getraws, true);

            //var_dump($params);
            $firstName = $customer->getFirstname();
            $lastName = $customer->getLastname();
            $region = $params['region'];
            $regionid = $params['region_id'];
            $code = $params['postcode'];
            $city = $params['city'];
            $street = $params['street'];
            $location = $params['location'];
            $phone = $params['telephone'];
            $vat = $params['vat_id'];

        $shippingAddress = array(
            'firstname' => $firstName,
            'middlename' => '',
            'lastname' => $lastName,
            'street' => array(
                '0' => $street, // compulsory
                '1' => $location // optional
            ),
            'city' => $city,
            'country_id' => 'ID', // two letters country code
            'region' => $region, // can be empty '' if no region
            'region_id' => $regionid, // can be empty '' if no region_id
            'postcode' => $code,
            'telephone' => $phone,
            'fax' => '',
            'vat_id' => $vat,
            'save_in_address_book' => 1
        );

        $address = Mage::getModel('customer/address');  
        $address->setData($shippingAddress)
            // ->setFirstname($customer->getFirstname())
            // ->setLastname($customer->getLastname())
            // ->setCountryId('ID')
            // ->setPostcode($code)
            // ->setCity($city)
            // ->setRegion($region)
            // ->setTelephone($phone)
            // ->setStreet($street)
            ->setCustomerId($customer->getId())
            // ->setVatId($vat)
            ->setIsDefaultBilling('1')
            ->setIsDefaultShipping('1')
            ->setSaveInAddressBook('1');
        $address->save();

        $this->_setAddressToCart($address);

        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Address successfully created.',
            'id' => $address->getId()
        );

        $this->_sendAPI($message_code);
        // $this->_redirect('checkout/onepage');
    }

    public function CartAction(){
        $params = $this->getRequest()->getParams();
            
        $productid = $params['product'];
        $productQty = $params['qty'];

        $customer = $this->_getCustomer();

        $cart = Mage::getSingleton('checkout/cart');
        $cart->init();
        $options = array('92'=>'49','144'=>'21');
        $product = Mage::getModel('catalog/product')->load($productid);

        $paramater = array('product' => $productid,
                            'qty' => $productQty,
                            'form_key' => Mage::getSingleton('core/session')->getFormKey(),
                            'options' => array('option_id'=>'sub_option_id')
                    );

        $request = new Varien_Object();
        $request->setData($paramater);
        $cart->addProduct($product, $request);
        $cart->save();

        $quote = Mage::helper('checkout/cart')->getCart()->getQuote();
        $quote->assignCustomer($customer);
        $quote->save();

        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Product added to cart',
            'quote' => $quote->getId(),
            'cart' => $cart->getQuote()->getId()
            // 'quote' => '396'
        );

        $this->_sendAPI($message_code);
    }


    public function QuoteAction(){

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        $customer = $this->_getCustomer();

       $quote = Mage::helper('checkout/cart')->getCart()->getQuote();

        // $quote = Mage::getModel('sales/quote')
        // ->setSharedStoreIds($storeIds)
        // ->loadByCustomer($customer);
        
        $value = [];
        foreach ($quote->getAllItems() as $item) {
           
            if ($item->getParentItemId()) {
                continue;
            }

            $product = $item->getProduct();

            $simpleProduct =  $item->getOptionByCode('simple_product')->getProduct();
            

            $image = (string)Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize(150,200);
               

            $value[]= array (
                'id' => $item->getItemId(),
                'product' => $product->getId(),
                'name' => $simpleProduct->getName(),
                
                'quantity' => $item->getQty(),
                'product_quantity' =>  $simpleProduct->getStockItem()->getQty() * 1,
                'price' => $item->getParentItemId()?  $item->getParentItem()->getPrice(): $item->getPrice(),
                'base_image' => $image,
                'thumbnail' => $image,
                'small_image' => $image,
            );

        }


        if($customer->getData('customer_photo_url') == null){
            $photo = null;
        }
        else {
            $photo = 'https://wia.id/media/host'.$customer->getData('customer_photo_url');
        }
        $customerarray = array(
            'id' => $customer->getId(),
            'firstname' => $customer->getFirstname(),
            'lastname' => $customer->getLastname(),
            'email' => $customer->getEmail(),
            'photo' => $photo
        ); 

        $quote = Mage::getModel("checkout/session")->getQuote();
        //$cart = Mage::getSingleton('checkout/cart');
        // $address = $cart->getQuote()->getShippingAddress();
        $address = $quote->getShippingAddress();
     

        if($address->getRegionId() == null){
            $getaddress = $customer->getPrimaryShippingAddress();
            if($getaddress == null){
                $customeraddress == null;
            }
            else {
                $this->_setAddressToCart($getaddress);
                // $quote = Mage::getModel("checkout/session")->getQuote();
                // $address = $quote->getShippingAddress();
                $customeraddress = array(
                    'id' => $address->getAddressId(),
                    'customer_id' => $customer->getId(),
                    'firstname' => $customer->getFirstname(),
                    'lastname' => $customer->getLastname(),
                    'email' => $customer->getEmail(),
                    'company' => $address->getCompany(),
                    'city' => $address->getCity(),
                    'region' => $address->getRegion(),
                    'postcode' => $address->getPostcode(),
                    'country_id' => $address->getCountryId(),
                    'telephone' => $address->getTelephone(),
                    'region_id' => $address->getRegionId(),
                    'street' => $address->getStreet(1),
                    'location' => $address->getStreet(2),
                    'vat_id' => $address->getData('vat_id')
                ); 
                foreach($address->getAllShippingRates() as $rate){
                    if($rate->getCode() == 'flatrate_flatrate'){}
                    else {
                        $array[] = array(
                            'carrier' => $rate->getCarrier(),
                            'carrier_title' => $rate->getCarrierTitle(),
                            'code' => $rate->getCode(),
                            'method' => $rate->getMethod(),
                            'method_description' => $rate->getMethodDescription(),
                            'price' => $rate->getPrice(),
                            'error_message' => $rate->getErrorMessage(),
                            'method_title' => $rate->getMethodTitle(),
                            'carrierName' => $rate->getCarrierName()
                        );
                    }
                }
            }
        }
        else {
            $customeraddress = array(
                'id' => $address->getAddressId(),
                'customer_id' => $customer->getId(),
                'firstname' => $customer->getFirstname(),
                'lastname' => $customer->getLastname(),
                'email' => $customer->getEmail(),
                'company' => $address->getCompany(),
                'city' => $address->getCity(),
                'region' => $address->getRegion(),
                'postcode' => $address->getPostcode(),
                'country_id' => $address->getCountryId(),
                'telephone' => $address->getTelephone(),
                'region_id' => $address->getRegionId(),
                'street' => $address->getStreet(1),
                'location' => $address->getStreet(2),
                'vat_id' => $address->getData('vat_id')
            );

            if($address->getShippingAmount() == 0){
                $this->_setAddressToCart($address);
            }
      
           
            foreach($address->getAllShippingRates() as $rate){
                if($rate->getCode() == 'flatrate_flatrate'){}
                else {
                    $array[] = array(
                        'carrier' => $rate->getCarrier(),
                        'carrier_title' => $rate->getCarrierTitle(),
                        'code' => $rate->getCode(),
                        'method' => $rate->getMethod(),
                        'method_description' => $rate->getMethodDescription(),
                        'price' => $rate->getPrice(),
                        'error_message' => $rate->getErrorMessage(),
                        'method_title' => $rate->getMethodTitle(),
                        'carrierName' => $rate->getCarrierName()
                    );
                }
            }
        }
        
        $shipping = $quote->getShippingAddress()->getShippingAmount();
        $subtotal = $quote->getSubtotal();
        $grand = $quote->getGrandTotal();
        

        if($value == null) {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Quotes null'
            );
        }
        else {
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Get Quotes Success',
                'customer' => $customerarray,
                'address' => $customeraddress,
                'quote' => $value,
                'id' => $quote->getId(),
                'shipping_name' => Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingDescription(),
                'code' => $shippingMethod =  Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod(),
                'shipping' => $shipping,
                'shipping_list' =>  $array,
                'promo' => $subtotal - $grand + $shipping,
                'subtotal' => $subtotal,
                'total' => $grand
            );
        }

      
        $this->_sendAPI($message_code);
    }

    public function QuoteDevelopAction(){

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

         $customer = Mage::getModel('customer/customer')->load(21);
      

       //$quote = Mage::helper('checkout/cart')->getCart()->getQuote();

        $quote = Mage::getModel('sales/quote')
        ->setSharedStoreIds($storeIds)
        ->loadByCustomer($customer);
        
        $value = [];
        foreach ($quote->getAllItems() as $item) {
           
            if ($item->getParentItemId()) {
                continue;
            }

            $product = $item->getProduct();

            $simpleProduct =  $item->getOptionByCode('simple_product')->getProduct();
            

            $image = (string)Mage::helper('catalog/image')->init($item->getProduct(), 'small_image')->resize(150,200);
               

            $value[]= array (
                'id' => $item->getItemId(),
                'product' => $product->getId(),
                'name' => $simpleProduct->getName(),
                
                'quantity' => $item->getQty(),
                'product_quantity' =>  $simpleProduct->getStockItem()->getQty() * 1,
                'price' => $item->getParentItemId()?  $item->getParentItem()->getPrice(): $item->getPrice(),
                'base_image' => $image,
                'thumbnail' => $image,
                'small_image' => $image,
            );

        }


        if($customer->getData('customer_photo_url') == null){
            $photo = null;
        }
        else {
            $photo = 'https://wia.id/media/host'.$customer->getData('customer_photo_url');
        }
        $customerarray = array(
            'id' => $customer->getId(),
            'firstname' => $customer->getFirstname(),
            'lastname' => $customer->getLastname(),
            'email' => $customer->getEmail(),
            'photo' => $photo
        ); 

        $quote = Mage::getModel("checkout/session")->getQuote();
        //$cart = Mage::getSingleton('checkout/cart');
        // $address = $cart->getQuote()->getShippingAddress();
        $address = $quote->getShippingAddress();
     

        if($address->getRegionId() == null){
            $getaddress = $customer->getPrimaryShippingAddress();
            if($getaddress == null){
                $customeraddress == null;
            }
            else {
                $this->_setAddressToCart($getaddress);
                // $quote = Mage::getModel("checkout/session")->getQuote();
                // $address = $quote->getShippingAddress();
                $customeraddress = array(
                    'id' => $address->getAddressId(),
                    'customer_id' => $customer->getId(),
                    'firstname' => $customer->getFirstname(),
                    'lastname' => $customer->getLastname(),
                    'email' => $customer->getEmail(),
                    'company' => $address->getCompany(),
                    'city' => $address->getCity(),
                    'region' => $address->getRegion(),
                    'postcode' => $address->getPostcode(),
                    'country_id' => $address->getCountryId(),
                    'telephone' => $address->getTelephone(),
                    'region_id' => $address->getRegionId(),
                    'street' => $address->getStreet(1),
                    'location' => $address->getStreet(2),
                    'vat_id' => $address->getData('vat_id')
                ); 
                foreach($address->getAllShippingRates() as $rate){
                    if($rate->getCode() == 'flatrate_flatrate'){}
                    else {
                        $array[] = array(
                            'carrier' => $rate->getCarrier(),
                            'carrier_title' => $rate->getCarrierTitle(),
                            'code' => $rate->getCode(),
                            'method' => $rate->getMethod(),
                            'method_description' => $rate->getMethodDescription(),
                            'price' => $rate->getPrice(),
                            'error_message' => $rate->getErrorMessage(),
                            'method_title' => $rate->getMethodTitle(),
                            'carrierName' => $rate->getCarrierName()
                        );
                    }
                }
            }
        }
        else {
            $customeraddress = array(
                'id' => $address->getAddressId(),
                'customer_id' => $customer->getId(),
                'firstname' => $customer->getFirstname(),
                'lastname' => $customer->getLastname(),
                'email' => $customer->getEmail(),
                'company' => $address->getCompany(),
                'city' => $address->getCity(),
                'region' => $address->getRegion(),
                'postcode' => $address->getPostcode(),
                'country_id' => $address->getCountryId(),
                'telephone' => $address->getTelephone(),
                'region_id' => $address->getRegionId(),
                'street' => $address->getStreet(1),
                'location' => $address->getStreet(2),
                'vat_id' => $address->getData('vat_id')
            );

            if($address->getShippingAmount() == 0){
                $this->_setAddressToCart($address);
            }
      
           
            foreach($address->getAllShippingRates() as $rate){
                if($rate->getCode() == 'flatrate_flatrate'){}
                else {
                    $array[] = array(
                        'carrier' => $rate->getCarrier(),
                        'carrier_title' => $rate->getCarrierTitle(),
                        'code' => $rate->getCode(),
                        'method' => $rate->getMethod(),
                        'method_description' => $rate->getMethodDescription(),
                        'price' => $rate->getPrice(),
                        'error_message' => $rate->getErrorMessage(),
                        'method_title' => $rate->getMethodTitle(),
                        'carrierName' => $rate->getCarrierName()
                    );
                }
            }
        }
        
        $shipping = $quote->getShippingAddress()->getShippingAmount();
        $subtotal = $quote->getSubtotal();
        $grand = $quote->getGrandTotal();
        

        if($value == null) {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Quotes null'
            );
        }
        else {
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Get Quotes Success',
                'customer' => $customerarray,
                'address' => $customeraddress,
                'quote' => $value,
                'id' => $quote->getId(),
                'shipping_name' => Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingDescription(),
                'code' => $shippingMethod =  Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod(),
                'shipping' => $shipping,
                'shipping_list' =>  $array,
                'promo' => $subtotal - $grand + $shipping,
                'subtotal' => $subtotal,
                'total' => $grand
            );
        }

      
        $this->_sendAPI($message_code);
    }

 

    public function createOrderAction(){
        
        // if (!$this->_validateFormKey()) {
        //     $this->_redirect('/success');
        //     return;
        // }

        $getraws = $this->getRequest()->getRawBody();
        $params = json_decode($getraws, true);

        $recepient = $params['recepient'];
        $message = $params['message'];

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        $customer = $customer = $this->_getCustomer();
        // $formKeys = Mage::getSingleton('core/session')->getFormKey();
        
        $paymentMethod = 'snap';
        // $paymentMethod = 'banktransfer';

        $shippingMethod =  Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod();
        // $shippingMethod = 'flatrate_flatrate';

        $quote = Mage::helper('checkout/cart')->getCart()->getQuote();
        // $quote = Mage::getModel('sales/quote')->load($quoteId);
        $quote->getAllItems();

        // $shippingMethod = $quote->getShippingMethod();

        // Set currency for the quote
        $quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode()); 

        // Assign customer to quote
        $quote->assignCustomer($customer);

        $billingAddressData = $quote->getBillingAddress();//->addData($customerBillingAddress);
        $shippingAddressData = $quote->getShippingAddress();//->addData($customerShippingAddress);

         // Set shipping and payment method on quote shipping address data
         $shippingAddressData->setShippingMethod($shippingMethod)
         ->setPaymentMethod($paymentMethod);

        // Set payment method for the quote
        $quote->getPayment()->importData(array('method' => $paymentMethod));


        try {
            // Collect totals of the quote
            $quote->collectTotals();
             
            // Save quote
            $quote->save();
                
            // Create Order From Quote
            $service = Mage::getModel('sales/service_quote', $quote);
            $service->submitAll();
            $incrementId = $service->getOrder()->getRealOrderId();
            $orderId = $service->getOrder()->getId();
                
 
            Mage::getSingleton('checkout/session')
            //    ->setQuoteId('123')
            //    ->setLastRealOrderId('123');
                ->setRealOrderId($incrementId)
                ->setLastRealOrderId($incrementId);
 
 
            //set comment
            $order = Mage::getModel('sales/order')->load($orderId);
            //  $order->sendNewOrderEmail();
            //  $order->addStatusHistoryComment('Booking Date : '.$date.'</br>'.'Booking Time '.$time);
            $order->save();

            if(!empty($recepient) && !empty($message)){
                $gift = $this->_gift($order, $recepient, $message);
            }
            else {}
 
            $result['success'] = true;
            $result['error'] = false;
             
            // $redirecturl = Mage::getBaseUrl().'snapio/payment/redirect';
             
            //$response['response'] = $this->myAjax();
            // $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            // $this->getResponse()->setRedirect($redirecturl);
            // $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result)); 

            $result = Mage::helper('Wia_Module_helper')->Payment($incrementId);

            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Order has been created',
                'order_id' => $order->getId(),
                'payment' => $result,
                'gift_message' => $gift
            );
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
           // $subscriber = Mage::getModel('advancednewsletter/subscriber')->subscribe($customer->getEmail(), 'buyer_customer','');
            $this->_clearsession();
    
            // $this->_sendAPI($message_code); 
                 
            } catch (Mage_Core_Exception $e) {
                //$redirecturl = Mage::getBaseUrl().'snapbarugopay/payment/redirect';
                $result['success'] = false;
                $result['error'] = true;
                $result['error_messages'] = $e->getMessage();
                $message_code = array( 
                    'message_code' => 400,
                    'message_dialog' => 'Order cannot be create, '.$e->getMessage()
                );
                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($message_code));
                 
                if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                    Mage::getSingleton('checkout/session')->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($message));
                    }
                }
            } 
            catch (Exception $e) {
                $result['success']  = false;
                $result['error']    = true;
                $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                 
                Mage::logException($e);
                //$this->_goBack();
            } 
    }

    public function PayAction(){
        $params = $this->getRequest()->getParams();
        $orderIncrementId = $params['order_id'];

        $result = Mage::helper('Wia_Module_helper')->Payment($orderIncrementId);
        
      
        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Order has been created',
            'order_id' => $orderIncrementId,
            'payment' => $result
        );
        $this->_sendAPI($message_code);
    }

    public function ShippingListAction(){

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        $customer = $this->_getCustomer();
        $customerShippingAddress = $customer->getPrimaryShippingAddress();
        // Update the cart's quote.
        $cart = Mage::getSingleton('checkout/cart');
        $address = $cart->getQuote()->getShippingAddress();


        $address->setCountryId($customerShippingAddress->getCountryId())
                ->setPostcode($customerShippingAddress->getPostcode())
                ->setCity($customerShippingAddress->getCity())
                ->setRegionId($customerShippingAddress->getReqionId())
                ->setRegion($region)
                ->setCollectShippingrates(true);
        $cart->save();

        // Find if our shipping has been included.
        $rates = $address->collectShippingRates()
        ->getGroupedAllShippingRates();

        $shipping = $address->collectShippingRates()->getGroupedAllShippingRates();

        if($shipping == null){
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Shipping List Null'
            );
        }
        else {
            foreach($shipping as $methods => $rates){
                foreach ($rates as $rate){
                    if($rate['code'] == 'flatrate_flatrate'){}
                    else {
                        $array[] = array(
                            'carrier' => $rate['carrier'],
                            'carrier_title' => $rate['carrier_title'],
                            'code' => $rate['code'],
                            'method' => $rate['method'],
                            'method_description' => $rate['method_description'],
                            'price' => $rate['price'],
                            'error_message' => $rate['error_message'],
                            'method_title' => $rate['method_title'],
                            'carrierName' => $rate['carrierName']
                        );
                    }
                }
            }
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'GET Shipping List success',
                'id' => $cart->getQuote()->getId(),
                'shipping' => $array
                // 'id' => '396',
                // 'shipping' => $shipping
            );
        }
        $this->_sendAPI($message_code); 
    }

    public function SetShippingAction(){

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        $params = $this->getRequest()->getParams();
        $code = $params['code'];

        $cart = Mage::getSingleton('checkout/cart');

        $address = $cart->getQuote()->getShippingAddress();
        $address
                ->setShippingMethod($code)
                ->setCollectShippingrates(true);
        $cart->save();

        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Set Shipping Method Success',
            // 'quote' => $quote->getId(),
            'shipping' => Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod()
            // 'quote' => '396',
            // 'shipping' => 'flatrate_flatrate'
        );

        $this->_sendAPI($message_code);
    }

    public function GetShippingAction(){

        if(Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod() == 'flatrate_flatrate'){
            $cart = Mage::getSingleton('checkout/cart');
            $code = 'wearinasiapickuppoint_mycarrier_freeoneday';
            // $code = 'freeshipping_freeshipping';

            $address = $cart->getQuote()->getShippingAddress();
            $address->setShippingMethod($code)->setCollectShippingrates(true);
            $cart->save();
        }
        else {}

        $currentShippingName = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingDescription();
        $currentShippingAmount = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingAmount();

        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'GET Shipping success',
            'shipping' => $currentShippingName,
            'shipping_amount' => $currentShippingAmount
            // ,
            // 'code' => Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod()
        );
        $this->_sendAPI($message_code); 
    }

    public function ClearAction(){
        // Clear Magento cart and quote items
        $cart = Mage::getModel('checkout/cart');                
        $cart->truncate()->save(); // remove all active items in cart page
        $cart->init();
        $session= Mage::getSingleton('checkout/session');
        $customer = $this->_getCustomer();
        $quote = Mage::getModel('sales/quote')->loadByCustomer($customer->getId());
        // $quote = $session->getQuote();
        $cart = Mage::getModel('checkout/cart');
        $cartItems = $cart->getItems();
        foreach ($cartItems as $item)
        {
            $quote->removeItem($item->getId())->save();
        }
        Mage::getSingleton('checkout/session')->clear();
        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Clear Cart Success'
        );
        $this->_sendAPI($message_code);
    }

    public function VoucherAction(){
        if ($_SERVER['REQUEST_METHOD'] === 'GET'){
            $coll = Mage::getResourceModel('salesrule/rule_collection')
            ->addFieldToFilter('is_rss', 1)
            ->load();
            
            // $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
            $customer = $this->_getCustomer();
            $quote = Mage::getModel('sales/quote')->loadByCustomer($customer->getId());
            $items = $quote->getAllItems();
            foreach($items as $item){ 
                foreach($coll as $rule) {
                    if ($rule->getActions()->validate($item)){
                        $promo = Mage::getModel('salesrule/rule')->load($rule->getId());
                        if(!empty($promo->getCouponCode())){
                            $array[] = array(
                                'description' => $promo->getDescription(),
                                'code' => $promo->getCouponCode(),
                                'discount' => $promo->getDiscountAmount()
                            );
                        } 
                    }
                }
            }
            // $array[] = array(
            //     'description' => 'lalaland',
            //     'code' => 'TESTING',
            //     'discount' => 20000
            // );
            $test = $this->unique_multidim_array($array, 'code');
            if(!empty($test)){
                $message_code = array( 
                    'message_code' => 200,
                    'message_dialog' => 'Get Promo Codes Success',
                    'promo' => $test
                );
            }
            else {
                $message_code = array( 
                    'message_code' => 400,
                    'message_dialog' => 'No Promo Code',
                    'promo' => $test
                );
            }
            $this->_sendAPI($message_code);
        }
        else if ($_SERVER['REQUEST_METHOD'] === 'POST'){
            $store = Mage::app()->getStore();
            $website = Mage::app()->getWebsite();

            $params = $this->getRequest()->getParams();
            $code = $params['code'];
            $customer = $this->_getCustomer();

            Mage::getSingleton('checkout/cart')->getQuote()
            ->setCouponCode($code)->collectTotals()->save();

            $rule =  Mage::getSingleton('checkout/session')->getQuote()->getAppliedRuleIds();
            // $quote = Mage::getModel('sales/quote')->loadByCustomer($customer->getId());
            $quote = Mage::helper('checkout/cart')->getCart()->getQuote();

            $value = [];
            foreach ($quote->getAllItems() as $item) {
                $value[]= array (
                    'id' => $item->getProductId(),
                    'name' => $item->getName(),
                    'quantity' => $item->getQty(),
                    'price' => $item->getParentItemId()?  $item->getParentItem()->getPrice(): $item->getPrice()
                    // 'id' => '2',
                    // 'name' => 'Samsung Galaxy Note 10+',
                    // 'quantity' => 1,
                    // 'price' => '100000.0000'
                );
            }
            $shipping = $quote->getShippingAddress()->getShippingAmount();
            $subtotal = $quote->getSubtotal();
            $grand = $quote->getGrandTotal();
            // $quote->setShippingMethod('flatrate_flatrate')->save();
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Promo Applied',
                'quote' => $value,
                'id' => $quote->getId(),
                'shipping_method' => $shippingMethod =  Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod(),
                'shipping' => $shipping,
                'promo' => $subtotal - $grand + $shipping,
                'subtotal' => $subtotal,
                'total' => $grand
                // 'id' => '396',
                // 'shipping_method' => 'flatrate_flatrate',
                // 'shipping' => '100.0000',
                // 'subtotal' => '100000.0000',
                // 'total' => '100100.0000'
            );

            $this->_sendAPI($message_code);
        }
        else {
            $message_code = array('message_code' => 405, 'message_dialog' => 'Method Not Allowed');
            $this->_sendAPI($message_code);
        }
        
    }

    public function CityListAction(){
        $params = $this->getRequest()->getParams();
        $region = $params['region'];
        
        $url    = 'https://wia.id/aongkir/index/city?prov_id='.$region;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);        
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        // curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
            $result     = curl_exec($curl);
            $array = Mage::helper('core')->jsondecode($result);
            curl_close($curl);

        if($array == null) {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'City List is null'
            );
        }
        else {
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Get City List Success',
                'city_list' => $array
            );
        }
        $this->_sendAPI($message_code);
    }

    public function RegionListAction(){
        $params = $this->getRequest()->getParams();
        $country = $params['country'];

        // $regionlist = array(
        //     'Bali',
        //     'Banka Belitung',
        //     'Banten',
        //     'Bengkulu',
        //     'Dj Yogyakarta',
        //     'DKI Jakarta',
        //     'Gorontalo',
        //     'Jambi',
        //     'Jawa Barat',
        //     'Jawa Tengah',
        //     'Jawa Timur',
        //     'Kalimantan Barat',
        //     'Kalimantan Selatan',
        //     'Kalimantan Tengah',
        //     'Kalimantan Timur',
        //     'Kalimantan Utara',
        //     'Kepulauan Riau',
        //     'Lampung',
        //     'Maluku Utara',
        //     'Maluku',
        //     'Nanggroe Aceh Darussalam (NAD)',
        //     'Nusa Tenggara Barat (NTB)',
        //     'Nusa Tenggara Timur (NTT)',
        //     'Papua Barat',
        //     'Papua',
        //     'Riau',
        //     'Sulawesi Barat',
        //     'Sulawesi Selatan',
        //     'Sulawesi Tengah',
        //     'Sulawesi Tenggara',
        //     'Sulawesi Utara',
        //     'Sumatera Barat',
        //     'Sumatera Selatan',
        //     'Sumatera Utara'
        // );

        $regions = Mage::getModel('directory/region')->getResourceCollection()
                ->addCountryFilter($country)
                ->load();

        foreach($regions as $regionlist){
            $array[] = array (
                'region_id' => $regionlist->getRegionId(),
                'country_id' => $regionlist->getCountryId(),
                'code' => $regionlist->getCode(),
                'default_name' => $regionlist->getDefaultName(),
                'name' => $regionlist->getName()
            );
        }
        // $this->_sendAPI($array);


        if($array == null) {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Region List is null'
            );
        }
        else {
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Get Region List Success',
                'region_list' => $array
            );
        }
        $this->_sendAPI($message_code);
    }

    public function ProceedAction(){
        
    }

    public function DeleteProductAction(){

        $id = (int)$this->getRequest()->getParam('product');
        if ($id) {
            try {
                $cartHelper = Mage::helper('checkout/cart');

                $items = $cartHelper->getCart()->getItems();
                
                $cartHelper->getCart()->removeItem( $id )->save();
                Mage::getSingleton('checkout/session')->setCartWasUpdated(true);

                $message_code = array( 
                    'message_code' => 200,
                    'message_dialog' => 'Remove Item Success'
                );
                $this->_sendAPI($message_code);
            } catch (Exception $e) {
                $message_code = array( 
                    'message_code' => 400,
                    'message_dialog' => 'Cannot Remove Item'
                );
                $this->_sendAPI($message_code);
            }
        }
    }


    public function UpdateCartAction(){

        $id = (int)$this->getRequest()->getParam('product');
        $qty = $this->getRequest()->getParam('qty');
        
        if ($id) {
            try {
                $cartHelper = Mage::helper('checkout/cart');
                
                // $items = $cartHelper->getCart()->getItems();
                $cart = Mage::getSingleton('checkout/cart');
                $item = $cart->getQuote()->getItemById($id);

                $item->setQty($qty);
                $item->save();   

                $cartHelper->getCart()->save();
                Mage::getSingleton('checkout/session')->setCartWasUpdated(true);

                $message_code = array( 
                    'message_code' => 200,
                    'message_dialog' => 'Update Cart Success'
                );
                $this->_sendAPI($message_code);
            } catch (Exception $e) {
                $message_code = array( 
                    'message_code' => 400,
                    'message_dialog' => 'Cannot Update Cart'
                );
                $this->_sendAPI($message_code);
            }
        }

    }

    public function postAddressAction(){
            
        // if (!$this->_validateFormKey()) {
        //     $this->_redirect('*/*');
        //     return;
        // }

            //login customer data
            $websiteId = Mage::app()->getWebsite()->getId();
            $store = Mage::app()->getStore();
            $customer = $this->_getCustomer();
        
            $getraws = $this->getRequest()->getRawBody();
            $params = json_decode($getraws, true);

            //var_dump($params);
            $firstName = $customer->getFirstname();
            $lastName = $customer->getLastname();
            $region = $params['region'];
            $regionid = $params['region_id'];
            $code = $params['postcode'];
            $city = $params['city'];
            $street = $params['street'];
            $location = $params['location'];
            $phone = $params['telephone'];
            $vat = $params['vat_id'];

        $shippingAddress = array(
            'firstname' => $firstName,
            'middlename' => '',
            'lastname' => $lastName,
            'street' => array(
                '0' => $street, // compulsory
                '1' => $location // optional
            ),
            'city' => $city,
            'country_id' => 'ID', // two letters country code
            'region' => $region, // can be empty '' if no region
            'region_id' => $regionid, // can be empty '' if no region_id
            'postcode' => $code,
            'telephone' => $phone,
            'fax' => '',
            'vat_id' => $vat,
            'save_in_address_book' => 1
        );

        $address = Mage::getModel('customer/address');  
        $address->setData($shippingAddress)
            // ->setFirstname($customer->getFirstname())
            // ->setLastname($customer->getLastname())
            // ->setCountryId('ID')
            // ->setPostcode($code)
            // ->setCity($city)
            // ->setRegion($region)
            // ->setTelephone($phone)
            // ->setStreet($street)
            ->setCustomerId($customer->getId())
            // ->setVatId($vat)
            ->setIsDefaultBilling('1')
            ->setIsDefaultShipping('1')
            ->setSaveInAddressBook('1');
        $address->save();

        $customeraddress = array(
            'id' => $address->getId(),
            'customer_id' => $customer->getId(),
            'firstname' => $customer->getFirstname(),
            'lastname' => $customer->getLastname(),
            'email' => $customer->getEmail(),
            'company' => $address->getCompany(),
            'city' => $address->getCity(),
            'region' => $address->getRegion(),
            'postcode' => $address->getPostcode(),
            'country_id' => $address->getCountryId(),
            'telephone' => $address->getTelephone(),
            'region_id' => $address->getRegionId(),
            'street' => $address->getStreet(1),
            'location' => $address->getStreet(2),
            'vat_id' => $address->getData('vat_id')
        );

        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Address successfully created.',
            'id' => $address->getId(),
            'address' => $customeraddress
        );

        $this->_sendAPI($message_code);
        // $this->_redirect('checkout/onepage');
    }

    public function editAddressAction(){
        $getparam = $this->getRequest()->getParams();
        $addressId = $getparam['id'];

        $customer = $this->_getCustomer();
        // $address = $customer->getPrimaryShippingAddress();
        $address = Mage::getModel("customer/address")->load($addressId);
    
        $getraws = $this->getRequest()->getRawBody();
        $params = json_decode($getraws, true);

        // $customerFirstname = $params['firstname'];
        // $customerLastname = $params['lastname'];
        $customerCompany = $params['company'];
        $customerTelephone = $params['telephone'];
        $customerCity = $params['city'];
        $customerRegion = $params['region'];
        $customerRegionId = $params['region_id'];
        $customerPostalcode = $params['postcode'];
        $customerCountryId = $params['country_id'];   
        $customerStreet = $params['street'];
        $location = $params['location'];
        $vat = $params['vat_id']; 

        $address->setCountryId($customerCountryId);
        $address->setPostcode($customerPostalcode);
        $address->setCity($customerCity);
        $address->setTelephone($customerTelephone);
        $address->setCompany($customerCompany);
        $address->setStreet($customerStreet);
        $address->setRegion($customerRegion);
        $address->setRegionId($customerRegionId);
        $address->setData('vat_id', $vat);
        // $address->setData();
        // $address->setStreet(1, $customerStreet);
        // $address->setStreet(2, $location);
        $address->save();

        $customeraddress = array(
            'id' => $address->getId(),
            'customer_id' => $customer->getId(),
            'firstname' => $customer->getFirstname(),
            'lastname' => $customer->getLastname(),
            'email' => $customer->getEmail(),
            'company' => $address->getCompany(),
            'city' => $address->getCity(),
            'region' => $address->getRegion(),
            'postcode' => $address->getPostcode(),
            'country_id' => $address->getCountryId(),
            'telephone' => $address->getTelephone(),
            'region_id' => $address->getRegionId(),
            'street' => $address->getStreet(1),
            'location' => $address->getStreet(2),
            'vat_id' => $address->getData('vat_id')
        );
        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'Address Edited',
            'address' => $customeraddress
        );

        $this->_sendAPI($message_code);
    }

    public function GiftAction(){

        $quote = Mage::helper('checkout/cart')->getCart()->getQuote();
        $customer = $customer = $this->_getCustomer();

        $getraws = $this->getRequest()->getRawBody();
        $params = json_decode($getraws, true);

        // $params = $this->getRequest()->getParams();

        $recepient = $params['recepient'];
        $message = $params['message'];

        if(!empty($recepient) && !empty($message)){
            if($quote->getGiftMessageId() != 'null'){
                $giftMessage = Mage::getModel('giftmessage/message')->load($quote->getGiftMessageId()); 
                $giftMessage->setCustomerId($customer->getId()); 
                $giftMessage->setSender($customer->getFirstname().' '.$customer->getLastname()); 
                $giftMessage->setRecipient($recepient); 
                $giftMessage->setMessage($message); 
                $giftObj = $giftMessage->save(); 

                $quote->setGiftMessageId($giftObj->getId()); 
                $quote->save();

                if(!empty($giftMessage)){
                    $message_code = array( 
                        'message_code' => 200,
                        'message_dialog' => 'Gift Message has been edited',
                        'gift_message' => $giftMessage->getData()
                    );
                }
                else {
                    $message_code = array( 
                        'message_code' => 400,
                        'message_dialog' => 'Failed to edit gift message',
                        'gift_message' => $giftMessage->getData()
                    );
                }
            }
            else {
                $giftMessage = Mage::getModel('giftmessage/message'); 
                $giftMessage->setCustomerId($customer->getId()); 
                $giftMessage->setSender($customer->getFirstname().' '.$customer->getLastname()); 
                $giftMessage->setRecipient($recepient); 
                $giftMessage->setMessage($message); 
                $giftObj = $giftMessage->save(); 

                $quote->setGiftMessageId($giftObj->getId()); 
                $quote->save();

                if(!empty($giftMessage)){
                    $message_code = array( 
                        'message_code' => 200,
                        'message_dialog' => 'Gift Message has been created',
                        'gift_message' => $giftMessage->getData()
                    );
                }
                else {
                    $message_code = array( 
                        'message_code' => 400,
                        'message_dialog' => 'Failed to make gift message',
                        'gift_message' => $giftMessage->getData()
                    );
                }
            }
        }
        else {
            $message_code = array( 
                'message_code' => 403,
                'message_dialog' => 'Message and Recepient are required'
            );
        } 
        $this->_sendAPI($message_code);
    }

    public function _gift($data, $data1, $data2){
        // $quote = Mage::helper('checkout/cart')->getCart()->getQuote();
        $customer = $customer = $this->_getCustomer();

        $order = $data;

        // $getraws = $this->getRequest()->getRawBody();
        // $params = json_decode($getraws, true);

        // $params = $this->getRequest()->getParams();

        // $recepient = $params['recepient'];
        // $message = $params['message'];

        $recepient = $data1;
        $message = $data2;

        if(!empty($recepient) && !empty($message)){
            
                $giftMessage = Mage::getModel('giftmessage/message'); 
                $giftMessage->setCustomerId($customer->getId()); 
                $giftMessage->setSender($customer->getFirstname().' '.$customer->getLastname()); 
                $giftMessage->setRecipient($recepient); 
                $giftMessage->setMessage($message); 
                $giftObj = $giftMessage->save(); 

                $order->setGiftMessageId($giftObj->getId()); 
                $order->save();

                if(!empty($giftMessage)){
                    return $order->getGiftMessageId();
                }
                else {
                    $message_code = array( 
                        'message_code' => 400,
                        'message_dialog' => 'Failed to make gift message',
                        'gift_message' => $giftMessage->getData()
                    );
                }
            
        }
        else {
            $message_code = array( 
                'message_code' => 403,
                'message_dialog' => 'Message and Recepient are required'
            );
        } 
        // $this->_sendAPI($message_code);
    }

    public function GetGiftAction(){

        $params = $this->getRequest()->getParams();
        $id = $params['id'];
        
        $giftMessage = Mage::getModel('giftmessage/message')->load($id); 

        if(!empty($giftMessage)){
            $message_code = array( 
                'message_code' => 200,
                'message_dialog' => 'Get Gift Message Success',
                'gift_message' => $giftMessage->getData()
            );
        }
        else {
            $message_code = array( 
                'message_code' => 400,
                'message_dialog' => 'Failed to get gift message',
                'gift_message' => $giftMessage->getData()
            );
        }

        $this->_sendAPI($message_code);
    }

    public function hahaAction(){
        // $customer = $this->_getCustomer();
        // $customerShippingAddress = $customer->getPrimaryShippingAddress();
        // Update the cart's quote.
        $cart = Mage::getSingleton('checkout/cart');
        $address = $cart->getQuote()->getShippingAddress()->getData('vat_id');
        
        $this->_sendAPI($address);
    }

    public function heheAction(){
        if(Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod() == 'flatrate_flatrate'){
            $cart = Mage::getSingleton('checkout/cart');
            $code = 'wearinasiapickuppoint_mycarrier_freeoneday';
            // $code = 'freeshipping_freeshipping';

            $address = $cart->getQuote()->getShippingAddress();
            $address->setShippingMethod($code)->setCollectShippingrates(true);
            $cart->save();
        }
        else {}

        $currentShippingName = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingDescription();
        $currentShippingAmount = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingAmount();

        $message_code = array( 
            'message_code' => 200,
            'message_dialog' => 'GET Shipping success',
            'shipping' => $currentShippingName,
            'shipping_amount' => $currentShippingAmount
            // ,
            // 'code' => Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod()
        );
        $this->_sendAPI($message_code); 
    }
}

?>
