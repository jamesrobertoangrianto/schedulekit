<?php
    class Wia_Module_BookingController extends Mage_Core_Controller_Front_Action
    {

        public function _sendAPI($data){
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
            return true;
        }
    
        public function _validate(){
            return true;
        }

        public function addNotificationAction(){
            
            
            $id = $this->getRequest()->getParam('id');
            $type = $this->getRequest()->getParam('type');
            
            if($type=='order'){
                $order = Mage::getModel('sales/order')->load($id);
                $cutomer =  $this->_getCustomerById($order);
                $product = $this->_getProducts($order);
                $business = $this->_getBusinessById($order->getBusinessId());
    
                $response = array( 
                    'status_code' => 200,
                    'id' =>  $order->getId(),
                    'increment_id' => $order->getIncrementId(),
                    'business_id' => $order->getBusinessId(),
                    'assign_member_id' => $order->getAssignMemberId(),
                    'status' => $order->getStatus(),
                    'status_label' => $order->getStatusLabel(),
                    'created_date' => $order->getCreatedAt(),
                    'from_date' => $order->getFromDate(),
                    'from_time' => $order->getFromTime(),
                    'to_date' => $order->getToDate(),
                    'to_time' => $order->getToTime(),
                    'booking_channel' => $order->getBookingChannel(),
                    'grand_total' => Mage::helper('core')->currency($order->getBaseGrandTotal(), true, false),
    
                    'product_name'=>$product[0]['name'],
    
                    'customer_name'=>$cutomer['name'],
                    'customer_email'=>$cutomer['email'],
                 
                    'business_name'=>$business['name'],
                    'business_address'=>$business['address']['street'],
                    'business_region'=>$business['address']['region'],
                    'business_city'=>$business['address']['city'],
                    'business_phone'=>$business['address']['telephone'],
    
                    'action_url'=>'https://getnotify.id/admin/booking/'.$order->getBusinessId().'/view/booking_id/'.$order->getId()
                    
                );
                $this->_sendMail(1,$response,$cutomer['name'],$cutomer['email']);
                $this->_sendMail(2,$response,$business['name'],$business['address']['email']);
               
            }

            if($type=='review'){
                // $order = Mage::getModel('sales/order')->load($id);
                // $cutomer =  $this->_getCustomerById($order);
                // $product = $this->_getProducts($order);
                $business = $this->_getBusinessById($order->getBusinessId());
    
                $response = array( 
                    'status_code' => 200,
                    // 'id' =>  $order->getId(),
                    // 'increment_id' => $order->getIncrementId(),
                    // 'business_id' => $order->getBusinessId(),
                    // 'assign_member_id' => $order->getAssignMemberId(),
                    // 'status' => $order->getStatus(),
                    // 'status_label' => $order->getStatusLabel(),
                    // 'created_date' => $order->getCreatedAt(),
                    // 'from_date' => $order->getFromDate(),
                    // 'from_time' => $order->getFromTime(),
                    // 'to_date' => $order->getToDate(),
                    // 'to_time' => $order->getToTime(),
                    // 'booking_channel' => $order->getBookingChannel(),
                    // 'grand_total' => Mage::helper('core')->currency($order->getBaseGrandTotal(), true, false),
    
                    // 'product_name'=>$product[0]['name'],
    
                    // 'customer_name'=>$cutomer['name'],
                    // 'customer_email'=>$cutomer['email'],
                 
                    'business_name'=>$business['name'],
                    // 'business_address'=>$business['address']['street'],
                    // 'business_region'=>$business['address']['region'],
                    // 'business_city'=>$business['address']['city'],
                    // 'business_phone'=>$business['address']['telephone'],
    
                    'action_url'=>'https://getnotify.id/admin/review/'.$order->getBusinessId().'/view/booking_id/'.$order->getId()
                    
                );
                $this->_sendMail(1,$response,$cutomer['name'],$cutomer['email']);
               
            }
           


            
            
            $this->_sendAPI($response);


        }

        public function _sendMail($templateId,$var,$recepientName,$recepientEmail){
            
            try {
                // Enter you new template ID
                $senderName = Mage::getStoreConfig('trans_email/ident_support/name');  //Get Sender Name from Store Email Addresses
                $senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');  //Get Sender Email Id from Store Email Addresses
                $sender = array('name' => $senderName,'email' => $senderEmail);
                
                $store = Mage::app()->getStore()->getId();
                
                Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, $recepientEmail, $recepientName, $var, $storeId);
                
            } catch (Mage_Core_Exception $e) {
                //throw $th;
            }
                
        }

        public function _sendConfirmationMail($id){
            
            $order = Mage::getModel('sales/order')->load($id);
            
            $cutomer =  $this->_getCustomerById($order);
            $product = $this->_getProducts($order);
            $business = $this->_getBusinessById($order->getBusinessId());

            $response = array( 
                'id' =>  $order->getId(),
                'increment_id' => $order->getIncrementId(),
                'business_id' => $order->getBusinessId(),
                'assign_member_id' => $order->getAssignMemberId(),
                'status' => $order->getStatus(),
                'status_label' => $order->getStatusLabel(),
                'created_date' => $order->getCreatedAt(),
                'from_date' => $order->getFromDate(),
                'from_time' => $order->getFromTime(),
                'to_date' => $order->getToDate(),
                'to_time' => $order->getToTime(),
                'booking_channel' => $order->getBookingChannel(),
                'grand_total' => Mage::helper('core')->currency($order->getBaseGrandTotal(), true, false),

                'product_name'=>$product[0]['name'],

                'customer_name'=>$cutomer['name'],
                'customer_email'=>$cutomer['email'],
             
                'business_name'=>$business['name'],
                'business_address'=>$business['address']['street'],
                'business_region'=>$business['address']['region'],
                'business_city'=>$business['address']['city'],
                'business_phone'=>$business['address']['telephone'],

                'action_url'=>'https://getnotify.id/admin/booking/'.$order->getBusinessId().'/view/booking_id/'.$order->getId()
                
            );


            $this->_sendMail(1,$response,$cutomer['name'],$cutomer['email']);
            $this->_sendMail(2,$response,$business['name'],$business['address']['email']);
            //$this->_sendAPI($business );

        }

        public function _getBusinessById($id){
            
            $business = Mage::getModel('Module/business')->load($id);
            $response = array( 
                'id' => $business->getBusinessId(),
                //'business_owner_id' => $business->getBusinessOwnerId(),
                'name' => $business->getName(),
                'type' => $business->getType(),
                'category' => $business->getCategory(),
                'short_description' => $business->getShortDescription(),
                'description' => $business->getDescription(),
                //'social_media' => $this->_getSocialMedia($busines),

                //entitiy db
                'address' => $this->_getBusinessAddressById($business->getBusinessId()),
                //entitiy db
                //'photos' => $this->_getGalleryListByBusinessId($business->getBusinessId()),
                //entitiy db
                //'attribute' => $this->_getBusinessAttributeById($business->getBusinessId()),
               // 'customer' => $this->_getCustomerById($business->getBusinessOwnerId()),

            );
            return $response;
        }

        public function _getBusinessAddressById($business_id){
            
            $address = Mage::getModel('Module/address');
            $address->load($business_id,'business_id');
          
            $response = array( 

                'id' =>  $address->getAddressId(),
                'business_id' =>  $address->getBusinessId(),
                'street' => $address->getStreet(),
                'telephone' => $address->getTelephone(),
                'email' => $address->getEmail(),
                'city' => $address->getCity(),
                'region' => $address->getRegion(),
                'website' => $address->getWebsite(),
                'coordinates' => $address->getCoordinates(),
            );
          
            return $response;
        }

       


        public function _getCustomerById($order){
            
            $response = array( 
                'id' => $order->getCustomerId(),
                'name' => $order->getBillingAddress()->getFirstname(),
                'email' => $order->getCustomerEmail(),
                'phone' => $order->getBillingAddress()->getTelephone(),
                
            );
            return $response;
        }

        public function _getProducts($order){
            foreach ($order->getAllVisibleItems() as $item) {
                $product = $item->getProduct();
                $response[]= array( 
                    'id' =>  $item->getProductId(),
                    'name' => $item->getName(),
                    'price' => $item->getName(),
                    'image' =>   (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(100,100),
                   
                );
              
               
             }
            
            return $response;
        }


        public function _getBookingById($id){

            $order = Mage::getModel('sales/order')->load($id);
            
            $response = array( 
                'id' =>  $order->getId(),
                'increment_id' => $order->getIncrementId(),
                'business_id' => $order->getBusinessId(),
                'assign_member_id' => $order->getAssignMemberId(),
                'status' => $order->getStatus(),
                'status_label' => $order->getStatusLabel(),
                'created_date' => $order->getCreatedAt(),
                'from_date' => $order->getFromDate(),
                'from_time' => $order->getFromTime(),
                'to_date' => $order->getToDate(),
                'to_time' => $order->getToTime(),
                'booking_channel' => $order->getBookingChannel(),
                'grand_total' => $order->getBaseGrandTotal(),
                'customer' =>  $this->_getCustomerById($order), 
                'products' => $this->_getProducts($order),
                
            );
            return $response;
            //var_dump( $order );
        }

        public function _SearchByIncrementId($business_id,$id){

            $order = Mage::getModel('sales/order')->loadByIncrementId($id);
            if($order->getId() && $business_id==$order->getBusinessId()){
                $response[] = array( 
                    'id' =>  $order->getId(),
                    'increment_id' => $order->getIncrementId(),
                    'business_id' => $order->getBusinessId(),
                    'assign_member_id' => $order->getAssignMemberId(),
                    'status' => $order->getStatus(),
                    'status_label' => $order->getStatusLabel(),
                    'created_date' => $order->getCreatedAt(),
                    'from_date' => $order->getFromDate(),
                    'from_time' => $order->getFromTime(),
                    'to_date' => $order->getToDate(),
                    'to_time' => $order->getToTime(),
                    'booking_channel' => $order->getBookingChannel(),
                    'grand_total' => $order->getBaseGrandTotal(),
                    'customer' =>  $this->_getCustomerById($order), 
                    'products' => $this->_getProducts($order),
                    
                );
            }
            else{
                $response = null;
            }
            
            return $response;
            //var_dump( $order );
        }


        public function _getBookingList($business_id,$status){

            

            $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            //->addFieldToFilter('customer_id',  1)
           ->addFieldToFilter('business_id', $business_id  )
           ->addFieldToFilter('status', $status)
            ->setOrder('created_at', 'desc');

            foreach ($orders as $order){
                $response[]  = array( 
                    'id' => $order->getId(),
                    'business_id' => $order->getBusinessId(),
                    'increment_id' => $order->getIncrementId(),
                    'status' => $order->getStatus(),
                    'status_label' => $order->getStatusLabel(),
                    'created_date' => $order->getCreatedAt(),
                    'from_date' => $order->getFromDate(),
                    'from_time' => $order->getFromTime(),

                    'to_date' => $order->getToDate(),
                    'to_time' => $order->getToTime(),

                    'booking_channel' => $order->getBookingChannel(),
                    'grand_total' => $order->getGrandTotal(),
                    'products' => $this->_getProducts($order),
                    //'items' =>  $this->_getOrderItems($order),
                    'customer' =>  $this->_getCustomerById($order),                 
                );
            }

            return $response;
        }

        public function _getOrderItems($order){
            $orderItems = $order->getItemsCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('product_type', array('eq'=>'simple'))
                ->load();

                foreach ($orderItems as $item){
                    $product = $item->getProduct();
                    $response[] = array( 

                        'name'          => $item->getName(),
                        'sku'           => $item->getSku(),
                        'id'           => $item->getId(),
                        'price'         => $item->getPrice(),
                        'ordered_qty'   => $item->getQtyOrdered(),
                        'image'         => (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(100,100),
                    
                    );
                }

                
                
                
             return   $response;

        }

        

        

        


        public function viewAction(){
            //$this->_validate();
            $id = $this->getRequest()->getParam('id');
            $business_id = $this->getRequest()->getParam('business_id');
            $increment_id = $this->getRequest()->getParam('increment_id');
            
            
           
            if($id){
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                    'booking' => $this->_getBookingById($id),
                );
                
            }

            
            
            if($business_id){
                if($increment_id && $increment_id!==null){
                    $response = array( 
                        'status_code' => 200,
                        'message_dialog' => 'Success',
                        'booking' => $this->_SearchByIncrementId($business_id,$increment_id),
                    );  
                }
                else{
                    $from_date = $this->getRequest()->getParam('from_date');
                    $to_date = $this->getRequest()->getParam('to_date');
                    $status= $this->getRequest()->getParam('status');
                    if(!$from_date){
                        $from_date = Mage::getModel('core/date')->date('Y-m-d');;
                    }
                    if(!$status){
                        $status = 'pending';
                    }
                    
                    
                    $response = array( 
                        
                        'status_code' => 200,
                        'endpoint'=>'booking_list',
                        'message_dialog' => 'Success',
                        'from_date' => $from_date,
                        'to_date' => $to_date,
                        'booking_status' => $status,
                        'booking' => $this->_getBookingList($business_id,$status),
                    );
                }
                
            }

            $this->_sendAPI($response );
            
            
        }


     
        public function _addCustomerBooking($params){
            
            $response = array( 
                'status_code' => 200,
                'message_dialog' => 'Success',
                'params' =>$params,
                
            );

            $this->_sendAPI($response);

        }
        public function _addBusinessBooking($params){
            $response = array( 
                'status_code' => 200,
                'message_dialog' => 'Success',
                'params' =>$params,
                
            );

            $this->_sendAPI($response);
        }

        public function addAction(){
            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                 // $params = $this->getRequest()->getParams();

             $getraws = $this->getRequest()->getRawBody();
             $params = json_decode($getraws, true);
 

            //store infor
            $store = Mage::app()->getStore();
            $website = Mage::app()->getWebsite();
             
            
             
            // set customer information
            $customer_email = $params['customer_email'];
            $customer_firstname = $params['customer_name'];
            $customer_lastname = $params['customer_name'];
            $telephone =  $params['customer_phone'];
            $productId = $params['product_id'];
            $qty=1;

    

            // initialize sales quote object
            $quote = Mage::getModel('sales/quote')->setStoreId($storeId);
            
             //custom booking information
            $quote->setBusinessId($params['business_id']);
           // $quote->setBusinessId(1);
             $quote->setBookingChannel($params['booking_channel']);
             $quote->setFromDate($params['from_date']);
             $quote->setToDate($params['to_date']);
             $quote->setFromTime($params['from_time']);
             $quote->setToTime($params['to_time']);
             $quote->setAssignMemberId($params['assign_member_id']);

           


            //mapping data 
            $billingAddress = array(
                'customer_address_id' => '',
                'prefix' => '',
                'firstname' => $customer_firstname,
                'middlename' => '',
                'lastname' => $customer_lastname,
                'suffix' => '',
                'company' => '', 
                'street' => array(
                    '0' => '-', // required
                    '1' => '-' // optional
                ),
                'city' => '-',
                'country_id' => 'ID', // country code
                'region' => 'Indonesia',
                'region_id' => 'ID',
                'postcode' => '00000',
                'telephone' => $telephone,
            );
             
           
       

            $quote->setCheckoutMethod('guest')
            ->setCustomerId(null)
            ->setCustomerEmail($customer_email)
            ->setCustomerIsGuest(true)
            ->setCustomerGroupId(Mage_Customer_Model_Group::NOT_LOGGED_IN_ID);
             
            // set currency for the quote
            $quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode());
             
            
            $product = Mage::getModel('catalog/product')->load($productId);
            $quote->addProduct($product, $qty);
             
            // add billing address to quote
            $billingAddressData = $quote->getBillingAddress()->addData($billingAddress);
              
            // add shipping address to quote
            $shippingAddressData = $quote->getShippingAddress()->addData($billingAddress);
             
            // collect shipping rates on quote
            $shippingAddressData->setCollectShippingRates(true)
                                ->collectShippingRates();
              
            // set shipping method and payment method on the quote
            $shippingAddressData->setShippingMethod('freeshipping_freeshipping')
                                ->setPaymentMethod('purchaseorder');
             
            // Set payment method for the quote
            $quote->getPayment()->importData(array('method' => 'purchaseorder'));


            try {
                $quote->collectTotals();
                $quote->save();
                $service = Mage::getModel('sales/service_quote', $quote);
                $service->submitAll();
                $increment_id = $service->getOrder()->getRealOrderId();
                $booking_id = $service->getOrder()->getId();
                $business_id = $service->getOrder()->getBusinessId();

                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'successs',
                    'booking'=> array(
                        id=>$booking_id,
                        business_id=>$business_id,
                        increment_id=>$increment_id
                    )
                    
                );
               
               
                $success = true;


              
            } catch (Exception $e) {
               
                $response = array( 
                    'status_code' => 400,
                    'message_dialog' =>  $e,
                   
                );
               
                $success = false;
                
                
            }
                $this->_sendAPI($response);
                
               
            }
           
                
            
        }

         
        public function editAction(){
            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                $params = $this->getRequest()->getParams();
                if($params['status'] == 'PROCESSING'){
                    $state = Mage_Sales_Model_Order::STATE_PROCESSING;
                }
                if($params['status'] == 'CANCELED'){
                    $state = Mage_Sales_Model_Order::STATE_CANCELED;
                }
                if($params['status'] == 'COMPLETE'){
                    //$state = Mage_Sales_Model_Order::STATE_PROCESSING;
                    $status = 'complete_unpaid';
                }
                
                try {
                    $order = Mage::getModel('sales/order')->load($params['id']);
                        if($state){
                            $order->setState($state, true);
                        }
                        
                        if($status){
                            $order->setStatus($status, true);
                        }
                        
                        
                        $order->save();

                        $response = array( 
                            'status_code' => 200,
                            'message_dialog' => $message,
                            'booking' =>array(
                                status=> $order->getStatus(),
                                status_label=> $order->getStatusLabel(),
                                state=> $order->getState(),
                            ),
                            
                        );

                } 
                catch (Mage_Core_Exception $e){
                        $response = array( 
                            'status_code' => 400,
                            'message_dialog' => $e,
                            
                            
                        );
                }
                $this->_sendAPI($response);
            }
            
            
            
            
        }

        public function editScheduleAction(){
            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                $params = $this->getRequest()->getParams();
                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);
                $order = Mage::getModel('sales/order')->load($params['id']);
                
                try {
                    
                    $order->setFromDate($params['from_date']);
                    $order->setToTime($params['to_time']);
                    $order->save();
    
                    $booking = array( 
                    'id' =>  $order->getId(),
                    'increment_id' => $order->getIncrementId(),
                    'business_id' => $order->getBusinessId(),
                    'assign_member_id' => $order->getAssignMemberId(),
                    'status' => $order->getStatus(),
                    'status_label' => $order->getStatusLabel(),
                    'created_date' => $order->getCreatedAt(),
                    'from_date' => $order->getFromDate(),
                    'from_time' => $order->getFromTime(),
                    'to_date' => $order->getToDate(),
                    'to_time' => $order->getToTime(),
                        );
    
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                    'booking' => $booking,
                );
                
    
               } 
               catch (Mage_Core_Exception $e){
                    $response = array( 
                        'status_code' => 400,
                        'message_dialog' => $e,
                        
                        
                    );
               }
    
    
              
    
                $this->_sendAPI($response);
            }
            
            
            
        }

        



        public function deleteAction(){
            $id = $this->getRequest()->getParam('id');
            echo '123';

            $this->_sendAPI( $response);
            
            
        }

       

       

      
       
    }

?>
