<?php
    class Wia_Module_ProductController extends Mage_Core_Controller_Front_Action
    {

        public function _sendAPI($data){
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
        }
    
        public function _validate(){
            return true;
        }

        public function testAction(){
            $product = Mage::getModel("catalog/product")->load(113);
            $associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product) ;
            foreach ($associatedProducts as $aspro) {
                $products[]=$aspro->getData();
            }
            $response = array( 
                'status_code' => 200,
                'message_dialog' => 'Success',
                'product' => $products,
            );

            $this->_sendAPI( $response);


            // $product = Mage::getModel("catalog/product")->load(113);
           
            // $products_links = Mage::getModel('catalog/product_link_api');
            // $group_product_id = $product->getId();
            // $products_links->assign("grouped",$group_product_id,105);
            
        }
        

        public function _getCustomerById(){
            $response = array( 
                'id' => '3123',
                'name' => 'James',
                'email' => 'email',
                'phone' => 'phone',
                'photo_url' => 'https://via.placeholder.com/200x200',
                'last_active' => 'James',
            );
            return $response;
        }

       

        public function _getProducts(){
            $response[] = array( 
                'id' => '3123',
                'name' => 'James',
                'price' => '3124123',
               
            );
            return $response;
        }


        public function _getProductById($id){
            $product = Mage::getModel("catalog/product")->load($id);

            if($product->getTypeId()=='grouped'){
                $associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product) ;
                foreach ($associatedProducts as $aspro) {
                    $aproducts[]=$aspro->getId();
                }
            }
            
            
            if($product->getStatus() ==2 ){
                $status=false;
            }
            if($product->getStatus() ==1 ){
                $status=true;
            }
           
            $response = array( 
                'id' => $product->getId(),
                'business_id' => $product->getBusinessId(),
                // 'attribute_id' => 'attribute_id',
                //'product_type' => 'product_type',
                'name' => $product->getName(),
                'status' => $status,
                'short_description' => $product->getShortDescription(),
                'description' => $product->getDescription(),
                'price' => round($product->getPrice(), 0),
                'special_price' => $product->getSpecialPrice()? round($product->getSpecialPrice(), 0): null,
                'image_url' => 'https://via.placeholder.com/200x200',
                
                'business_id' => $product->getBusinessId(),
                'service_duration' => $product->getServiceDuration(),
                'associate_product' => $aproducts,
                'options' => '[]options',
                'assign_members' => array(
                    '0'=> $this->_getCustomerById(),
                     '1'=> $this->_getCustomerById(),
                ),
               
            );
            return $response;
        }


        public function _getProductList($business_id,$status,$type){

            $products = Mage::getModel("catalog/product")
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('type_id', $type)
            ->addAttributeToFilter('business_id', $business_id)
             ->setOrder('created_at', 'desc'); //sets the order by price

            if($status==1){
                $products->addAttributeToFilter('status', 1);
            }            
            
             
            foreach ($products as $product){
                if($product->getStatus() ==2 ){
                    $status=false;
                }
                if($product->getStatus() ==1 ){
                    $status=true;
                }

                if($product->getTypeId()=='grouped'){
                    $associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product) ;
                    foreach ($associatedProducts as $aspro) {
                        $aproducts[]=$aspro->getId();
                    }
                }
                $response[] = array( 
                    'id' => $product->getId(),
                    'product_type' => $type,
                    'business_id' => $product->getBusinessId(),
                    'service_duration' => $product->getServiceDuration(),
                    'name' => $product->getName(),
                    'price' => round($product->getPrice(), 0),
                    'special_price' => $product->getSpecialPrice()? round($product->getSpecialPrice(), 0): null,
                    'status' =>$status,
                    'image_url' => 'https://via.placeholder.com/200x200',
                    'short_description' => $product->getShortDescription(),
                    'description' => $product->getDescription(),
                    'associate_product' => $aproducts,
                    'options' => '[]options',
                    'team_members' => array(
                        '0'=> $this->_getCustomerById(),
                         '1'=> $this->_getCustomerById(),
                    ),
                );
                
            }

            
            return $response;
        }
        


        public function viewAction(){
            $this->_validate();
            $id = $this->getRequest()->getParam('id');
            $business_id = $this->getRequest()->getParam('business_id');
            $status = $this->getRequest()->getParam('status');
           
           
            if($id){
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                    'product' => $this->_getProductById($id),
                );
            }
            if($business_id){
                $date = $this->getRequest()->getParam('status');
                $type='simple';
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                    'product' => $this->_getProductList($business_id,$status,$type),
                );
            }

            $this->_sendAPI( $response);
            
            
        }

        public function viewCategoryAction(){
            $this->_validate();
            $id = $this->getRequest()->getParam('id');
            $business_id = $this->getRequest()->getParam('business_id');
            $status = $this->getRequest()->getParam('status');
           
           
            if($id){
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                    'category' => $this->_getProductById($id),
                );
            }
            if($business_id){
                $date = $this->getRequest()->getParam('status');
                $type='grouped';
                $response = array( 
                    'status_code' => 200,
                    'message_dialog' => 'Success',
                    'category' => $this->_getProductList($business_id,$status,$type),
                );
            }

            $this->_sendAPI( $response);
            
            
        }

        public function addAction()
        {
            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                $params = $this->getRequest()->getParams();
            $business_id = $params['business_id'];
            
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $product = Mage::getModel("catalog/product");
            
            if($business_id){
                try{
                    $product
                    ->setStoreId(0) 
                    ->setWebsiteIds(array(1)) 
                    ->setAttributeSetId(4) 
                    ->setTypeId('simple') 
                    //->setCreatedAt(strtotime('now')) 
                    ->setSku($business_id.'-'.strtotime('now'))
                    ->setBusinessId($business_id)
                    //->setWeight(0)
                    ->setStatus(2)
                    //->setTaxClassId(0)
                    ->setVisibility(4);       
                    // ->setPrice($price)
                    // ->setDescription($params['description'])
                    // ->setShortDescription($params['short_description'])
    
                    // ->setMediaGallery (array('images'=>array (), 'values'=>array ()))
                    //->addImageToMediaGallery('media/host/4094-0.jpg', array('image','thumbnail','small_image'), false, false)
                    // ->setStockData(array(
                    //             'use_config_manage_stock' => 0,
                    //             'manage_stock'=>0,
                    //         )
                    // );
     
                    $product->save();
                        $response = array( 
                        'status_code' => 200,
                        'message_dialog' => 'Susccess',
                        'product' => array(
                            id=>$product->getId(),
                            business_id=>$product->getBusinessId(),
                        )
                        );
                    $this->_sendAPI($response);
                    
                    }
               
                    catch(Exception $e){
    
                        $response = array( 
                            'status_code' => 400,
                            'message_dialog' => $e->getMessage(),
                            
                            );
                        $this->_sendAPI($response);
                   
                        
                    }
                }
            }

        }

        public function addCategoryAction()
        {
            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                $params = $this->getRequest()->getParams();
            $business_id = $params['business_id'];
            
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $product = Mage::getModel("catalog/product");
            
            if($business_id){
                try{
                    $product
                    ->setStoreId(0) 
                    ->setWebsiteIds(array(1)) 
                    ->setAttributeSetId(4) 
                    ->setTypeId('grouped') 
                    //->setCreatedAt(strtotime('now')) 
                    ->setSku($business_id.'-'.strtotime('now'))
                    ->setBusinessId($business_id)
                    //->setWeight(0)
                    ->setStatus(2)
                    //->setTaxClassId(0)
                    ->setVisibility(4);       
                    // ->setPrice($price)
                    // ->setDescription($params['description'])
                    // ->setShortDescription($params['short_description'])
    
                    // ->setMediaGallery (array('images'=>array (), 'values'=>array ()))
                    //->addImageToMediaGallery('media/host/4094-0.jpg', array('image','thumbnail','small_image'), false, false)
                    // ->setStockData(array(
                    //             'use_config_manage_stock' => 0,
                    //             'manage_stock'=>0,
                    //         )
                    // );
     
                    $product->save();
                        $response = array( 
                        'status_code' => 200,
                        'message_dialog' => 'Susccess',
                        'product' => array(
                            id=>$product->getId(),
                            business_id=>$product->getBusinessId(),
                        )
                        );
                    $this->_sendAPI($response);
                    
                    }
               
                    catch(Exception $e){
    
                        $response = array( 
                            'status_code' => 400,
                            'message_dialog' => $e->getMessage(),
                            
                            );
                        $this->_sendAPI($response);
                   
                        
                    }
                }
            }

        }

        public function editAction()
        {
        
            //$params = $this->getRequest()->getParams();
            
            $getraws = $this->getRequest()->getRawBody();
            $params = json_decode($getraws, true);
            
            if($params['status']==false){
                $status=2;
            }
            else{
                $status=1;
            }
           
            $business_id = $params['business_id'];
            $product_id=$params['id'];

            
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $product = Mage::getModel("catalog/product")->load($params['id']);
            if($business_id == $product->getBusinessId() &&$product->getId()){
                try{
                    $product
                    // ->setStoreId(0) 
                    // ->setWebsiteIds(array(1)) 
                    // ->setAttributeSetId(4) 
                    ->setName($params['name']) 
                    ->setStatus($status)
                    ->setServiceDuration($params['service_duration'])
                    //->setWeight(0)
                    //->setTaxClassId(0)
                    //->setVisibility(1);       
                    ->setPrice($params['price'])
                    ->setSpecialPrice($params['special_price'])
                    ->setDescription($params['description'])
                    ->setShortDescription($params['short_description']);
    
                    // ->setMediaGallery (array('images'=>array (), 'values'=>array ()))
                    //->addImageToMediaGallery('media/host/4094-0.jpg', array('image','thumbnail','small_image'), false, false)
                    // ->setStockData(array(
                    //             'use_config_manage_stock' => 0,
                    //             'manage_stock'=>0,
                    //         )
                    // );
     
                    $product->save();
                        $response = array( 
                        'status_code' => 200,
                        'message_dialog' => 'Success',
                        'product' => $product->toArray(),
                        );
                    $this->_sendAPI($response);
                    
                    }
               
                    catch(Exception $e){
    
                        $response = array( 
                            'status_code' => 400,
                            'message_dialog' => $e->getMessage(),
                            
                            );
                        $this->_sendAPI($response);
                   
                        
                    }
            }
            else{
                $response = array( 
                    'status_code' => 400,
                    'message_dialog' => 'Product Id n BID not available',
                    
                    ); 
                    $this->_sendAPI($response);
            }
            

        }

        public function editCategoryAction()
        {
        
            //$params = $this->getRequest()->getParams();
            
            $getraws = $this->getRequest()->getRawBody();
            $params = json_decode($getraws, true);
            
            if($params['status']==false){
                $status=2;
            }
            else{
                $status=1;
            }
           
            $business_id = $params['business_id'];
            $product_id=$params['id'];
            $associate_products=$params['associate_product'];

            
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $product = Mage::getModel("catalog/product")->load($params['id']);
            if($business_id == $product->getBusinessId() &&$product->getId()){
                try{
                    $product->setGroupedLinkData(array())->save();
                    $product
                    ->setName($params['name']) 
                    ->setStatus($status)
                    ->setServiceDuration($params['service_duration'])
                    ->setPrice($params['price'])
                    ->setSpecialPrice($params['special_price'])
                    ->setDescription($params['description'])
                   
                    ->setShortDescription($params['short_description']);
                    
                    $group_product_id = $product->getId();
                    $products_links = Mage::getModel('catalog/product_link_api');
                    foreach ($associate_products as $associate_product){
                        $products_links->assign("grouped",$group_product_id,$associate_product);
                    }

                    $product->setGroupedLinkData($data);
                    
                    $product->save();
                        $response = array( 
                        'status_code' => 200,
                        'message_dialog' => 'Success',
                        'product' => $product->toArray(),
                        );
                    $this->_sendAPI($response);
                    
                    }
               
                    catch(Exception $e){
    
                        $response = array( 
                            'status_code' => 400,
                            'message_dialog' => $e->getMessage(),
                            
                            );
                        $this->_sendAPI($response);
                   
                        
                    }
            }
            else{
                $response = array( 
                    'status_code' => 400,
                    'message_dialog' => 'Product Id n BID not available',
                    
                    ); 
                    $this->_sendAPI($response);
            }
            

        }

        public function deleteAction(){
           
                Mage::register('isSecureArea', 1);
                $params = $this->getRequest()->getParams();
                $product_id=$params['product_id'];
                $business_id=$params['business_id'];
                $product = Mage::getModel('catalog/product')->load($product_id);

                if($product_id&&$business_id){
                    $product = Mage::getModel("catalog/product")->load($product_id);
                    if($product->getId()&&$product->getBusinessId()==$business_id){
                        try{
                            $product->delete();
                            $response = array( 
                                'status_code' => 200,
                                'message_dialog' => 'success',
                                'product_id'=>$product_id,
                                'business_id'=>$business_id,
                                );
                            
                        }
                        catch(Exception $e){
                            $response = array( 
                                'status_code' => 200,
                                'message_dialog' => $e->getMessage(),
                                
                                );
                        }                  
                    }
                    else{
                        $response = array( 
                            'status_code' => 400,
                            'message_dialog' => 'not allow',
                            'product_id'=>$product_id,
                            'business_id'=>$business_id,
                            );    
                    }
                }
                else{
                    $response = array( 
                        'status_code' => 400,
                        'message_dialog' => 'not allow',
                        'product_id'=>$product_id,
                        'business_id'=>$business_id,
                        );    
                }
            
                $this->_sendAPI( $response);
            
            
            
        }
 
    }

?>
