<?php
    class Wia_Module_ScheduleController extends Mage_Core_Controller_Front_Action
    {

        public function _sendAPI($data){
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
        }
    
        public function _validate(){
            return true;
        }

        public function _getCustomerById($order){
            
            $response = array( 
                'id' => $order->getCustomerId(),
                'name' => $order->getBillingAddress()->getFirstname(),
                'email' => $order->getCustomerEmail(),
                'phone' => $order->getBillingAddress()->getTelephone(),
                
            );
            return $response;
        }

        public function _getProducts($order){
            foreach ($order->getAllVisibleItems() as $item) {
                $product = $item->getProduct();
                $response= array( 
                    'id' =>  $item->getProductId(),
                   
                    'name' => $item->getName(),
                    'price' => $item->getName(),
                    'image' =>   (string)Mage::helper('catalog/image')->init($product, 'small_image')->resize(100,100),
                   
                );
              
               
             }
            
            return $response;
        }


        public function _getScheduleById($id){

            $order = Mage::getModel('sales/order')->load($id);

            $response = array( 
                'id' => $order->getId(),
                'booking_id' => $order->getIncrementId(),
                'status' =>  $order->getStatus(),
                'type' => 'appointment',
                'is_all_day' => false,
                'created_date' => 'created_date',
                'from_date' => $order->getFromDate(),
                'from_time' =>  $order->getFromTime(),
               // 'product' => $this->_getProduct(),
                'customer' => $this->_getCustomerById($order),
               
            );
            return $response;
        }


        public function _getScheduleList($business_id,$from_date){
            
            $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('from_date',  $from_date)
            ->addFieldToFilter('business_id', $business_id)
            ->addFieldToFilter('state', 'processing')
            ->setOrder('from_time', 'asc');


           foreach ($orders as $order) {
            $response[] = array( 
                'id' =>  $order->getId(),
                'booking_id' => $order->getId(),
                'business_id' => $order->getBusinessId(),
                'status' =>  $order->getStatus(),
                'status_label' =>  $order->getStatusLabel(),
                'type' => 'appointment',
                'is_all_day' => false,
                'created_date' => 'created_date',
                'from_date' => $order->getFromDate(),
                'from_time' => $order->getFromTime(),
                'to_date' => $order->getToDate(),
                'to_time' => $order->getToTime(),
                'product' => $this->_getProducts($order),
                'customers' =>  $this->_getCustomerById($order)
               
            );
            }   
            
            return $response;
        }

        


        public function viewAction(){
            $this->_validate();
            
            $id = $this->getRequest()->getParam('id');
            $business_id = $this->getRequest()->getParam('business_id');
            $from_date = $this->getRequest()->getParam('from_date');
            
           //$business_id=10;
           
            if($id){
                $response = array( 
                    'message_code' => 200,
                    'message_dialog' => 'Success',
                    'schedule' => $this->_getScheduleById($id),
                );
                
            }
            if($business_id){
                if(!$from_date){    
                    $from_date = Mage::getModel('core/date')->date('Y-m-d');//$this->getRequest()->getParam('from_date');
                
                }
                $to_date = $this->getRequest()->getParam('to_date');
                $response = array( 
                    'message_code' => 200,
                    'message_dialog' => 'Success',
                    'from_date' => $from_date,
                    'to_date' => $to_date,
                    'schedule' => $this->_getScheduleList($business_id,$from_date),
                );
            }

            $this->_sendAPI( $response);
            
            
        }
        public function addAction(){
            $params = $this->getRequest()->getParams();
            // $business = Mage::getModel('Module/business');
            // $business->setBusinessOwnerId($params['business_owner_id']);
            // $business->save();

            $response = array( 
                'status_code' => 200,
                'message_dialog' => 'Success',
                'schedule' => $params,
                
            );

            $this->_sendAPI($response);
            
        }
        public function editAction(){
            $id = $this->getRequest()->getParam('id');
            echo '123';

            $this->_sendAPI( $response);
            
            
        }
        public function deleteAction(){
            $id = $this->getRequest()->getParam('id');
            echo '123';

            $this->_sendAPI( $response);
            
            
        }

       

      
       
    }

?>
